# -*- coding: utf-8 -*-

import xlsxwriter
import base64
from io import BytesIO
from odoo import models, fields


class ReportReporteGeneralReport(models.TransientModel):
    _name = 'report.reporte.general.report'
    _description = 'Wizard para mostrar reporte'

    xlxs_report = fields.Binary(string='Reporte')
    xlxs_report_filename = fields.Char(string='Reporte excel', size=64)


class ReportReporteGeneralWizard(models.TransientModel):
    _name = 'report.reporte.general.wizard'
    
    date_from = fields.Date(string='Fecha Inicio', required=True)
    date_to = fields.Date(string='Fecha Final', required=True)
    
    def get_report_reporte_general(self):
        filename = 'Reporte_general_' + str(self.date_from) + '_' + str(self.date_to) + '.xlsx'
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        # Estilos
        bold_fmt = workbook.add_format({'bold': True})
        center_fmt = workbook.add_format(({'align': 'center'}))
        header_fmt = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#C6EFCE'})
        
        worksheet1 = workbook.add_worksheet('Reporte General')
        
        payslip_ids = self.env['hr.payslip'].search([('date_from', '>=', self.date_from), ('date_from', '<=', self.date_to)])
        
        # Encabezados
        #worksheet1.set_column('A:A', 20)
        #worksheet1.write('A1', 'Primer Nombre', header_fmt)
        #worksheet1.write('B1', 'Primer Apellido', header_fmt)
        #worksheet1.write('C1', 'Segundo Apellido', header_fmt)
        worksheet1.set_column('A:AU', 30)
        worksheet1.write('A1', 'Contrato', header_fmt)
        worksheet1.set_column('C:C', 20)
        worksheet1.write('B1', 'Cedula de Ciudadania', header_fmt)
        worksheet1.write('C1', 'Ubicación de Trabajo', header_fmt)
        worksheet1.write('D1', 'Departamento.', header_fmt)
        worksheet1.write('E1', "Cargo", header_fmt)
        worksheet1.write('F1', 'Días Trabajados', header_fmt)
        worksheet1.write('G1', 'Sueldo Acumulado', header_fmt)
        worksheet1.write('H1', 'Aux. Transporte Acumulado', header_fmt)
        worksheet1.write('I1', 'Recargo Diurno', header_fmt)
        worksheet1.write('J1', 'Recargo Nocturno', header_fmt)
        worksheet1.write('K1', 'Hora Extra Diurna', header_fmt)
        worksheet1.write('L1', 'Hora Extra Nocturna', header_fmt)
        worksheet1.write('M1', 'Hora Extra Diurna Dominical', header_fmt)
        worksheet1.write('N1', 'Hora Extra Nocturna Dominical', header_fmt)
        worksheet1.write('O1', 'Vacaciones Pagadas', header_fmt)
        worksheet1.write('P1', 'Incapacidad Enfermedad', header_fmt)
        worksheet1.write('Q1', 'Incapacidad Laboral', header_fmt)
        worksheet1.write('R1', 'Ausencia Remunerada', header_fmt)
        worksheet1.write('S1', 'Ausencia No Remunerada', header_fmt)
        worksheet1.write('T1', 'Ausencia Por Luto', header_fmt)
        worksheet1.write('U1', 'Pagos Constitutivos', header_fmt)
        worksheet1.write('V1', 'Pagos No Constitutivos', header_fmt)
        worksheet1.write('W1', 'Total Devengado', header_fmt)
        worksheet1.write('X1', 'Pensión Empleado', header_fmt)
        worksheet1.write('Y1', 'Salud Empleado', header_fmt)
        worksheet1.write('Z1', 'Fondo Solidaridad Pensional', header_fmt)
        worksheet1.write('AA1', 'Rete. Fuente', header_fmt)
        worksheet1.write('AB1', 'AFC', header_fmt)
        worksheet1.write('AC1', 'Total Deducciones', header_fmt)
        worksheet1.write('AD1', 'Pago Empleado', header_fmt)
        worksheet1.write('AE1', 'Pensión Empresa', header_fmt)
        worksheet1.write('AF1', 'Salud Empresa', header_fmt)
        worksheet1.write('AG1', 'ARL', header_fmt)
        worksheet1.write('AH1', 'Pago Seguridad Social', header_fmt)
        worksheet1.write('AI1', 'SENA', header_fmt)
        worksheet1.write('AJ1', 'ICBF', header_fmt)
        worksheet1.write('AK1', 'Caja de Compensación', header_fmt)
        worksheet1.write('AL1', 'Pago Parafiscales', header_fmt)
        worksheet1.write('AM1', 'Cesantías', header_fmt)
        worksheet1.write('AN1', 'Int/Cesantías', header_fmt)
        worksheet1.write('AO1', 'Prima', header_fmt)
        worksheet1.write('AP1', 'Vacaciones por provisión', header_fmt)
        worksheet1.write('AQ1', 'Total Provisión', header_fmt)
        worksheet1.write('AR1', 'Costo Empresa', header_fmt)
        
        # Armar estructura con sumatorias por empleado
        employee_data = {}
        for p in payslip_ids:
            if p.employee_id.id not in employee_data:
                employee_data[p.employee_id.id] = {
                    #'primer_nombre': p.employee_id.address_home_id.firstname,
                    #'primer_apellido': p.employee_id.address_home_id.lastname,
                    #'segundo_apellido': p.employee_id.address_home_id.other_lastname,
                    'contracto': p.contract_id.name,
                    'cedula': p.employee_id.identification_id or '',
                    'ubicacion': p.employee_id.work_location or '',
                    'departamento': p.employee_id.department_id.name or '',
                    'cargo': p.employee_id.job_id.name or '',
                    'dias_trabajados': p.contract_id._get_dias_trabajados(p.contract_id, p),
                    'sueldo_acumulado': p.line_ids.filtered(lambda x: x.code == 'Sueldo').total,
                    'aux_transporte': p.line_ids.filtered(lambda x: x.code == 'Aux_Transporte').total,
                    'rd': p.line_ids.filtered(lambda x: x.code == 'HR_DIURNO_DO_FEST').total,
                    'rn': p.line_ids.filtered(lambda x: x.code == 'HR_NOC_DO_FEST').total,
                    'hed': p.line_ids.filtered(lambda x: x.code == 'HE_DIURNA').total,
                    'hen': p.line_ids.filtered(lambda x: x.code == 'HE_NOCTURNA').total,
                    'hedd': p.line_ids.filtered(lambda x: x.code == 'HE_DIURNO_DO_FEST').total,
                    'hend': p.line_ids.filtered(lambda x: x.code == 'HE_NOC_DO_FEST').total,
                    'vacaciones_pagadas': 0,
                    'incapacidad_enfermedad': p.line_ids.filtered(lambda x: x.code == 'EPS').total,
                    'incapacidad_laboral': p.line_ids.filtered(lambda x: x.code == 'Incapacidad_ARL').total,
                    'ausencia_remunerada': 0,
                    'ausencia_no_remunerada': p.line_ids.filtered(lambda x: x.code == 'Ausencia_No_Remunerada').total,
                    'ausencia_por_luto': p.line_ids.filtered(lambda x: x.code == 'ausencia_luto').total,
                    'pagos_constitutivos': p.line_ids.filtered(lambda x: x.code == 'Pago_constitutivo').total,
                    'pagos_no_constitutivos': p.line_ids.filtered(lambda x: x.code == 'Pago_no_constitutivo').total,
                    'total_devengado': p.line_ids.filtered(lambda x: x.code == 'Total_Devengado').total,
                    'pension_empleado': p.line_ids.filtered(lambda x: x.code == 'Pension').total,
                    'salud_empleado': p.line_ids.filtered(lambda x: x.code == 'Salud').total,
                    'fondo_solidaridad': p.line_ids.filtered(lambda x: x.code == 'FSP').total,
                    'retencion_fuente': p.line_ids.filtered(lambda x: x.code == 'RetencionF').total,
                    'afc': p.line_ids.filtered(lambda x: x.code == 'AFC').total,
                    'total_deducciones': p.line_ids.filtered(lambda x: x.code == 'Total_Deducciones').total,
                    'pago_empleado': p.line_ids.filtered(lambda x: x.code == 'Total_Pagar').total,
                    'pension_empresa': p.line_ids.filtered(lambda x: x.code == 'Pensión_Empleador').total,
                    'salud_empresa': p.line_ids.filtered(lambda x: x.code == 'Salud_Empleador').total,
                    'arl': p.line_ids.filtered(lambda x: x.code in ['ARL-I', 'ARL-II', 'ARL-III', 'ARL-IV', 'ARL-V'] and x.total > 0).total,
                    'seguridad_social': p.line_ids.filtered(lambda x: x.code == 'Total_Seguridad_Social').total,
                    'sena': p.line_ids.filtered(lambda x: x.code == 'SENA').total,
                    'icbf': p.line_ids.filtered(lambda x: x.code == 'ICBF').total,
                    'caja_compensacion': p.line_ids.filtered(lambda x: x.code == 'CCF').total,
                    'parafiscales': p.line_ids.filtered(lambda x: x.code == 'Parafiscales').total,
                    'cesantias': p.line_ids.filtered(lambda x: x.code == 'Cesantias_mensual').total,
                    'int_cesantias': p.line_ids.filtered(lambda x: x.code == 'ISC').total,
                    'prima': p.line_ids.filtered(lambda x: x.code == 'Prima').total,
                    'vacaciones': p.line_ids.filtered(lambda x: x.code == 'VacacionesProvisionadas').total,
                    'total_provicion': p.line_ids.filtered(lambda x: x.code == 'Total_Prestaciones_Sociales').total,
                    'costo_empresa': p.line_ids.filtered(lambda x: x.code == 'costo_empresa').total,
                }
            else:
                employee_data[p.employee_id.id]['dias_trabajados'] += p.contract_id._get_dias_trabajados(p.contract_id, p)
                employee_data[p.employee_id.id]['sueldo_acumulado'] += p.line_ids.filtered(lambda x: x.code == 'Sueldo').total
                employee_data[p.employee_id.id]['aux_transporte'] += p.line_ids.filtered(lambda x: x.code == 'Aux_Transporte').total
                employee_data[p.employee_id.id]['rd'] += p.line_ids.filtered(lambda x: x.code == 'HR_DIURNO_DO_FEST').total
                employee_data[p.employee_id.id]['rn'] += p.line_ids.filtered(lambda x: x.code == 'HR_NOC_DO_FEST').total
                employee_data[p.employee_id.id]['hed'] += p.line_ids.filtered(lambda x: x.code == 'HE_DIURNA').total
                employee_data[p.employee_id.id]['hen'] += p.line_ids.filtered(lambda x: x.code == 'HE_NOCTURNA').total
                employee_data[p.employee_id.id]['hedd'] += p.line_ids.filtered(lambda x: x.code == 'HE_DIURNO_DO_FEST').total
                employee_data[p.employee_id.id]['hend'] += p.line_ids.filtered(lambda x: x.code == 'HE_NOC_DO_FEST').total
                # employee_data[p.employee_id.id]['vacaciones_pagadas'] += p.line_ids.filtered(lambda x: x.code == 'Vacaciones').total
                employee_data[p.employee_id.id]['incapacidad_enfermedad'] += p.line_ids.filtered(lambda x: x.code == 'EPS').total
                employee_data[p.employee_id.id]['incapacidad_laboral'] += p.line_ids.filtered(lambda x: x.code == 'Incapacidad_ARL').total
                employee_data[p.employee_id.id]['ausencia_remunerada'] += 0
                employee_data[p.employee_id.id]['ausencia_no_remunerada'] += p.line_ids.filtered(lambda x: x.code == 'Ausencia_No_Remunerada').total
                employee_data[p.employee_id.id]['ausencia_por_luto'] += p.line_ids.filtered(lambda x: x.code == 'ausencia_luto').total
                employee_data[p.employee_id.id]['pagos_constitutivos'] += p.line_ids.filtered(lambda x: x.code == 'Pago_constitutivo').total
                employee_data[p.employee_id.id]['pagos_no_constitutivos'] += p.line_ids.filtered(lambda x: x.code == 'Pago_no_constitutivo').total
                employee_data[p.employee_id.id]['total_devengado'] += p.line_ids.filtered(lambda x: x.code == 'Total_Devengado').total
                employee_data[p.employee_id.id]['pension_empleado'] += p.line_ids.filtered(lambda x: x.code == 'Pension').total
                employee_data[p.employee_id.id]['salud_empleado'] += p.line_ids.filtered(lambda x: x.code == 'Salud').total
                employee_data[p.employee_id.id]['fondo_solidaridad'] += p.line_ids.filtered(lambda x: x.code == 'FSP').total
                employee_data[p.employee_id.id]['retencion_fuente'] += p.line_ids.filtered(lambda x: x.code == 'RetencionF').total
                employee_data[p.employee_id.id]['afc'] += p.line_ids.filtered(lambda x: x.code == 'AFC').total
                employee_data[p.employee_id.id]['total_deducciones'] += p.line_ids.filtered(lambda x: x.code == 'Total_Deducciones').total
                employee_data[p.employee_id.id]['pago_empleado'] += p.line_ids.filtered(lambda x: x.code == 'Total_Pagar').total
                employee_data[p.employee_id.id]['pension_empresa'] += p.line_ids.filtered(lambda x: x.code == 'Pensión_Empleador').total
                employee_data[p.employee_id.id]['salud_empresa'] += p.line_ids.filtered(lambda x: x.code == 'Salud_Empleador').total
                employee_data[p.employee_id.id]['arl'] += p.line_ids.filtered(lambda x: x.code in ['ARL-I', 'ARL-II', 'ARL-III', 'ARL-IV', 'ARL-V'] and x.total > 0).total
                employee_data[p.employee_id.id]['seguridad_social'] += p.line_ids.filtered(lambda x: x.code == 'Total_Seguridad_Social').total
                employee_data[p.employee_id.id]['sena'] += p.line_ids.filtered(lambda x: x.code == 'SENA').total
                employee_data[p.employee_id.id]['icbf'] += p.line_ids.filtered(lambda x: x.code == 'ICBF').total
                employee_data[p.employee_id.id]['caja_compensacion'] += p.line_ids.filtered(lambda x: x.code == 'CCF').total
                employee_data[p.employee_id.id]['parafiscales'] += p.line_ids.filtered(lambda x: x.code == 'Parafiscales').total
                employee_data[p.employee_id.id]['cesantias'] += p.line_ids.filtered(lambda x: x.code == 'Cesantias_mensual').total
                employee_data[p.employee_id.id]['int_cesantias'] += p.line_ids.filtered(lambda x: x.code == 'ISC').total
                employee_data[p.employee_id.id]['prima'] += p.line_ids.filtered(lambda x: x.code == 'Prima').total
                employee_data[p.employee_id.id]['vacaciones'] += p.line_ids.filtered(lambda x: x.code == 'Vacaciones').total
                employee_data[p.employee_id.id]['total_provicion'] += p.line_ids.filtered(lambda x: x.code == 'Total_Prestaciones_Sociales').total
                employee_data[p.employee_id.id]['costo_empresa'] += p.line_ids.filtered(lambda x: x.code == 'costo_empresa').total
        
        row, col = 1, 0
        for emp in employee_data.values():
            #worksheet1.write(row, col, emp.get('primer_nombre', ''))
            #worksheet1.write(row, col + 1, emp.get('primer_apellido', ''))
            #worksheet1.write(row, col + 2, emp.get('segundo_apellido', ''))
            worksheet1.write(row, col, emp.get('contracto', ''))
            worksheet1.write(row, col + 1, emp.get('cedula', ''))
            worksheet1.write(row, col + 2, emp.get('ubicacion', ''))
            worksheet1.write(row, col + 3, emp.get('departamento', ''))
            worksheet1.write(row, col + 4, emp.get('cargo', ''))
            worksheet1.write(row, col + 5, emp.get('dias_trabajados', ''))
            worksheet1.write(row, col + 6, emp.get('sueldo_acumulado', ''))
            worksheet1.write(row, col + 7, emp.get('aux_transporte', ''))
            worksheet1.write(row, col + 8, emp.get('rd', ''))
            worksheet1.write(row, col + 9, emp.get('rn', ''))
            worksheet1.write(row, col + 10, emp.get('hed', ''))
            worksheet1.write(row, col + 11, emp.get('hen', ''))
            worksheet1.write(row, col + 12, emp.get('hedd', ''))
            worksheet1.write(row, col + 13, emp.get('hend', ''))
            worksheet1.write(row, col + 14, emp.get('vacaciones_pagadas', ''))
            worksheet1.write(row, col + 15, emp.get('incapacidad_enfermedad', ''))
            worksheet1.write(row, col + 16, emp.get('incapacidad_laboral'))
            worksheet1.write(row, col + 17, emp.get('ausencia_remunerada'))
            worksheet1.write(row, col + 18, emp.get('ausencia_no_remunerada', ''))
            worksheet1.write(row, col + 19, emp.get('ausencia_por_luto', ''))
            worksheet1.write(row, col + 20, emp.get('pagos_constitutivos', ''))
            worksheet1.write(row, col + 21, emp.get('pagos_no_constitutivos', ''))
            worksheet1.write(row, col + 22, emp.get('total_devengado', ''))
            worksheet1.write(row, col + 23, emp.get('pension_empleado', ''))
            worksheet1.write(row, col + 24, emp.get('salud_empleado', ''))
            worksheet1.write(row, col + 25, emp.get('fondo_solidaridad', ''))
            worksheet1.write(row, col + 26, emp.get('retencion_fuente', ''))
            worksheet1.write(row, col + 27, emp.get('afc', ''))
            worksheet1.write(row, col + 28, emp.get('total_deducciones', ''))
            worksheet1.write(row, col + 29, emp.get('pago_empleado', ''))
            worksheet1.write(row, col + 30, emp.get('pension_empresa', ''))
            worksheet1.write(row, col + 31, emp.get('salud_empresa', ''))
            worksheet1.write(row, col + 32, emp.get('arl', ''))
            worksheet1.write(row, col + 33, emp.get('seguridad_social', ''))
            worksheet1.write(row, col + 34, emp.get('sena', ''))
            worksheet1.write(row, col + 35, emp.get('icbf', ''))
            worksheet1.write(row, col + 36, emp.get('caja_compensacion', ''))
            worksheet1.write(row, col + 37, emp.get('parafiscales', ''))
            worksheet1.write(row, col + 38, emp.get('cesantias', ''))
            worksheet1.write(row, col + 39, emp.get('int_cesantias', ''))
            worksheet1.write(row, col + 40, emp.get('prima', ''))
            worksheet1.write(row, col + 41, emp.get('vacaciones', ''))
            worksheet1.write(row, col + 42, emp.get('total_provicion', ''))
            worksheet1.write(row, col + 43, emp.get('costo_empresa'))
            row += 1

        workbook.close()
        record_id = self.env['report.reporte.general.report'].create({'xlxs_report': base64.encodestring(output.getvalue()), 'xlxs_report_filename': filename})
        return {'view_mode': 'form',
                'res_id': record_id.id,
                'res_model': 'report.reporte.general.report',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'target': 'new',
                }