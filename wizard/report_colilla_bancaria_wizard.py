# -*- coding: utf-8 -*-

import xlsxwriter
import base64
from io import BytesIO
from odoo import models, fields


class ReportColillaBancariaReport(models.TransientModel):
    _name = 'report.colilla.bancaria.report'
    _description = 'Wizard para mostrar reporte'

    xlxs_report = fields.Binary(string='Reporte')
    xlxs_report_filename = fields.Char(string='Reporte excel', size=64)


class ReportColillaBancariaWizard(models.TransientModel):
    _name = 'report.colilla.bancaria.wizard'
    
    date_from = fields.Date(string='Fecha Inicio', required=True)
    date_to = fields.Date(string='Fecha Final', required=True)
    
    def get_report_colilla_bancaria(self):
        filename = 'Colilla_bancaria_' + str(self.date_from) + '_' + str(self.date_to) + '.xlsx'
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        # Estilos
        bold_fmt = workbook.add_format({'bold': True})
        center_fmt = workbook.add_format(({'align': 'center'}))
        header_fmt = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#C6EFCE'})
        
        worksheet1 = workbook.add_worksheet('Colilla Bancaria')
        
        payslip_ids = self.env['hr.payslip'].search([('date_from', '>=', self.date_from), ('date_from', '<=', self.date_to)])
        
        # Encabezados
        worksheet1.set_column('A:A', 20)
        worksheet1.write('A1', 'Tipo de Identificación', header_fmt)
        worksheet1.set_column('B:B', 20)
        worksheet1.write('B1', 'Nit / CC del Beneficiario', header_fmt)
        worksheet1.set_column('C:C', 45)
        worksheet1.write('C1', 'Nombre Beneficiario', header_fmt)
        worksheet1.set_column('D:D', 30)
        worksheet1.write('D1', 'Código del Banco Destino', header_fmt)
        worksheet1.set_column('E:F', 10)
        worksheet1.write('E1', 'Tipo de Cuenta', header_fmt)
        worksheet1.write('F1', 'Número de Cuenta Beneficiario.', header_fmt)
        worksheet1.write('G1', "Total a Pagar", header_fmt)
        worksheet1.set_column('H:H', 12)
        worksheet1.write('H1', 'Email', header_fmt)
        worksheet1.write('I1', 'Teléfono Beneficiario', header_fmt)
        worksheet1.write('J1', 'Dirección del Beneficiario', header_fmt)
        
        # Información de Nóminas
        row, col = 1, 0
        for payslip in payslip_ids:
            worksheet1.write(row, col, dict(payslip.employee_id._fields['tipo_identificacion'].selection).get(payslip.employee_id.tipo_identificacion))
            worksheet1.write(row, col + 1, payslip.employee_id.nit_cc)
            worksheet1.write(row, col + 2, payslip.employee_id.name)
            worksheet1.write(row, col + 3, payslip.employee_id.bank_account_id.bank_id.bic or '')
            worksheet1.write(row, col + 4, dict(payslip.employee_id._fields['tipo_de_cuenta'].selection).get(payslip.employee_id.tipo_de_cuenta))
            worksheet1.write(row, col + 5, payslip.employee_id.bank_account_id.acc_number or '')
            worksheet1.write(row, col + 6, payslip.line_ids.filtered(lambda x: x.code =='Total_Pagar').total)
            worksheet1.write(row, col + 7, payslip.employee_id.work_email)
            worksheet1.write(row, col + 8, payslip.employee_id.work_phone)
            worksheet1.write(row, col + 9, payslip.employee_id.address_home_id.street)
            row += 1
        workbook.close()
        record_id = self.env['report.colilla.bancaria.report'].create({'xlxs_report': base64.encodestring(output.getvalue()), 'xlxs_report_filename': filename})
        return {'view_mode': 'form',
                'res_id': record_id.id,
                'res_model': 'report.colilla.bancaria.report',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'target': 'new',
                }