# -*- coding: utf-8 -*-

from odoo import models, fields


class ResCompany(models.Model):
    _inherit = 'res.company'
    
    parametro_ccf = fields.Float(string='Parámetro CCF')
    parametro_icbf = fields.Float(string='Parámetro ICBF')
    parametro_sena = fields.Float(string='Parámetro SENA')
    parametro_pension = fields.Selection([
        ('12', '12%'),
        ('16', '16%')
    ], string='$ Pensión por Ausencia', defaul='12')
    estructura_liquidacion_id = fields.Many2one('hr.payroll.structure', string='Estructura liquidación')
    