# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.osv import osv
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp

# Agrega campos EPS y Fondo de Pensiones en datos del empleado.

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    #eps_id = fields.Many2one('res.partner', 'EPS')
    #fp_id = fields.Many2one('res.partner', 'Fondo de Pensiones')
    #fc_id =fields.Many2one('res.partner', 'Fondo de Cesantías')
    #caja_compensacion_id =fields.Many2one('res.partner', 'Caja de Compensación')
    tipo_identificacion = fields.Selection([
        ('1', '01:CEDULA'),
        ('2', '02:CEDULA EXTRANJERIA'),
        ('3', '03:NIT'),
        ('4', '04:TARJETA DE IDENTIDAD'),
        ('5', '05:PASAPORTE'),
        ('11', '11:NIT PN')
    ], string='Tipo de Identificación', default='1')
    nit_cc = fields.Char(string='NIT CC')
    tipo_de_cuenta = fields.Selection([
        ('aho', 'AHO:CUENTA DE AHORROS'),
        ('cte', 'CTE:CUENTA CORRIENTE'),
        ('crt', 'CRT:CREDITO ROTATIVO')
    ], string='Tipo de Cuenta')
    # total_cesantias = fields.Float(string='Total Cesantias', default=0.00)
    #horas_extra_ids = fields.One2many('hr.employee.horas.extra', 'employee_id', string='Horas extra')
    # Ingresos Constitutivos
    #ingresos_constitutivos_ids = fields.One2many('hr.employee.ingresos.constitutivos', 'employee_id', string='Ingresos Constitutivos')
    # Ingresos No Constitutivos
    #ingresos_no_constitutivos_ids = fields.One2many('hr.employee.ingresos.no.constitutivos', 'employee_id', string='Ingresos No Constitutivos')
    # Deducciones
    #deducciones_ids = fields.One2many('hr.employee.deducciones', 'employee_id', string='Deducciones')
    # Prestamos
    #prestamos = fields.Float(string='Prestamo')
    # Calendario días festivos
    # holidays_calendar_id = fields.Many2one('hr.holidays.calendar', string='Calendario días festivos')
    # Auxilio Conectividad
    #auxilio_conectividad = fields.Float(string='Auxilio Conectividad')
    
    def consultar_ingresos_no_constitutivos(self, payslip, type):
        ingresos_ids = self.env['hr.employee.ingresos.no.constitutivos'].search([
            ('employee_id', '=', payslip.employee_id), 
            ('date', '>=', payslip.date_from), 
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if ingresos_ids:
            return sum(ingresos_ids.mapped('amount'))
        return 0

    def consultar_ingresos_constitutivos(self, payslip, type):
        ingresos_ids = self.env['hr.employee.ingresos.constitutivos'].search([
            ('employee_id', '=', payslip.employee_id), 
            ('date', '>=', payslip.date_from), 
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if ingresos_ids:
            return sum(ingresos_ids.mapped('amount'))
        return 0
    
    def consultar_horas_extra_recargos(self, payslip, type):
        item_ids = self.env['hr.employee.horas.extra'].search([
            ('employee_id', '=', payslip.employee_id),
            ('date', '>=', payslip.date_from),
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if item_ids:
            return sum(item_ids.mapped('qty'))
        return 0
    
    def consultar_horas_extra_recargos_reporte(self, payslip, type):
        item_ids = self.env['hr.employee.horas.extra'].search([
            ('employee_id', '=', payslip.employee_id.id),
            ('date', '>=', payslip.date_from),
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if item_ids:
            return sum(item_ids.mapped('qty'))
        return 0
    
    def consultar_deducciones(self, payslip, type):
        item_ids = self.env['hr.employee.deducciones.constitutivas'].search([
            ('employee_id', '=', payslip.employee_id),
            ('date', '>=', payslip.date_from),
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if item_ids:
            return sum(item_ids.mapped('amount'))
        return 0
    
    def consultar_deducciones_no_const(self, payslip, type):
        item_ids = self.env['hr.employee.deducciones.no.constitutivas'].search([
            ('employee_id', '=', payslip.employee_id),
            ('date', '>=', payslip.date_from),
            ('date', '<=', payslip.date_to),
            ('type', '=', type)
        ])
        if item_ids:
            return sum(item_ids.mapped('amount'))
        return 0
    
    
class HrEmployeeHorasExtra(models.Model):
    _name = 'hr.employee.horas.extra'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    type = fields.Selection([
        ('hed', 'Hora Extra Diurnas'),
        ('hen', 'Hora Extra Nocturna'),
        ('hrn', 'Hora Recargo Nocturno'),
        ('heddf', 'Hora Extra Diurna Dominical y Festivos'),
        ('hrddf', 'Hora Recargo Diurno Dominical y Festivos'),
        ('hendf', 'Hora Extra Nocturna Dominical y Festivos'),
        ('hrndf', 'Hora Recargo Nocturno Dominical y Festivos'),
    ], string='Tipo hora extra o recargo', required=True)
    date = fields.Date(string='Fecha', required=True)
    qty = fields.Float(string='Cantidad')


class HrEmployeeIngresosConstitutivos(models.Model):
    _name = 'hr.employee.ingresos.constitutivos'
    _description = 'Ingresos Constitutivos'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    type = fields.Selection([
        ('anticipo', 'Anticipo Salarial'),
        ('bon_pre', 'Bonificación Prestacional'),
        ('com', 'Comisiones'),
        ('idem', 'Indemnización'),
        ('otros', 'Otros Ingresos'),
        ('recargo', 'Recargo'),
        ('reintegro', 'Reintegro'),
        ('rodamiento', 'Rodamiento'),
        ('via', 'Viatico y Alojamiento')
    ], string='Tipo', required=True)
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')


class HrEmployeeIngresosNoConstitutivos(models.Model):
    _name = 'hr.employee.ingresos.no.constitutivos'
    _description = 'Ingresos No Constitutivos'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    type = fields.Selection([
        ('aux_ali', 'Auxilio de Alimentación'),
        ('aux_est', 'Auxilio de Estudio'),
        ('aux_mov', 'Auxilio de Movilización'),
        ('aux_sal', 'Auxilio de Salud'),
        ('bon_lib', 'Bonificación a Mera Libertad'),
        ('bon_ext', 'Bonificación Extralegal por Desempeño'),
        ('bon_no_pre', 'Bonificación No Prestacional'),
        ('bon_can', 'Bono Canasta'),
        ('educa', 'Educación'),
        ('hon', 'Honorarios'),
        ('lic_rem', 'Licencia Remunerada'),
        ('otros', 'Otros Ingresos No Prestacionales'),
        ('prima', 'Prima'),
        ('viatico', 'Viatico y Alojamiento'),
    ], string='Tipo', required=True)
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')


class HrEmployeeAuxilioConectividad(models.Model):
    _name = 'hr.employee.auxilio.conectividad'
    _description = 'Auxilio Conectividad'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')


class HrEmployeeDeduccionesConstitutivas(models.Model):
    _name = 'hr.employee.deducciones.constitutivas'
    _description = 'Deducciones Consitutivas'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    type = fields.Selection([
        ('avc_afc', 'Aportes Voluntarios a Cuentas AFC'),
        ('avc_avc', 'Aportes Voluntarios a Cuentas AVC'),
        ('avf_no_obl', 'Aportes Voluntarios a Fondos de Pensiones (No Oblogatorias)'),
        ('avf_obl', 'Aportes Voluntarios a Fondos de Pensiones Obligatorias'),
        ('cel', 'Celular'),
        ('deud', 'Deuda'),
        ('educ', 'Educación'),
        ('embargo', 'Embargo Fiscal'),
        ('libr', 'Libranza'),
        ('pag_ter', 'Pago Tercero'),
        ('plan_com', 'Plan Complementario de Salud'),
        ('sanc', 'Sanción'),
    ], string='Tipo', required=True)
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')


class HrEmployeeDeduccionesNoConstitutivas(models.Model):
    _name = 'hr.employee.deducciones.no.constitutivas'
    _description = 'Deducciones No Consitutivas'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    type = fields.Selection([
        ('otras', 'Otras Deducciones'),
    ], string='Tipo', required=True)
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')


class HrEmployeePrestamos(models.Model):
    _name = 'hr.employee.prestamos'
    _description = 'Prestamos'
    
    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, ondelete='cascade')
    date = fields.Date(string='Fecha', required=True)
    amount = fields.Float(string='Monto')
