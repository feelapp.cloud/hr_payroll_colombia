# -*- coding: utf-8 +-*

from odoo import models, fields


class HrLeave(models.Model):
    _inherit = 'hr.leave'
    
    non_attendance_leave = fields.Boolean(string='Ausencia automática')
    non_attendance_date = fields.Date(string='Fecha de ausencia')
    leave_type_related = fields.Selection(related='holiday_status_id.leave_type', string='Tipo Ausencia')


class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    leave_type = fields.Selection([('incapacidad_arl', 'Incapacidad Laboral ARL'),
                                   ('incapacidad_eps', 'Incapacidad Enfermedad EPS'),
                                   ('vacaciones_anuales', 'Vacaciones Anuales'),
                                   ('vacaciones_anticipadas', 'Vacaciones Anticipadas'),
                                   ('por_luto', 'Licencia Por Luto'),
                                   ('ausencia_remunerada', 'Ausencia Remunerada'),
                                   ('otro', 'Otro')], string='Tipo ausencia (Nomina)')
