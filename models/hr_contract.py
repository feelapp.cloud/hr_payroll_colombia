# -*- coding: utf-8 -*-

import pytz
from calendar import monthrange
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api
from odoo.exceptions import ValidationError

tipo_trabajador = [
    ('01', 'Dependiente'),
    ('02', 'Servicio domestico'),
    ('04', 'Madre comunitaria'),
    ('12', 'Aprendices del Sena en etapa lectiva'),
    ('18', 'Funcionarios públicos sin tope máximo de ibc'),
    ('19', 'Aprendices del SENA en etapa productiva'),
    ('20', 'Estudiantes (régimen especial ley 789 de 2002)'),
    ('21', 'Estudiantes de postgrado en salud'),
    ('22', 'Profesor de establecimiento particular'),
    ('23', 'Estudiantes aportes solo riesgos laborales'),
    ('30', 'Dependiente entidades o universidades públicas con régimen especial en salud'),
    ('31', 'Cooperados o pre cooperativas de trabajo asociado'),
    ('47', 'Trabajador dependiente de entidad beneficiaria del sistema general de participaciones - aportes patronales'),
    ('51', 'Trabajador de tiempo parcial'),
    ('54', 'Pre pensionado de entidad en liquidación'),
    ('55', 'Afiliado participe - dependiente'),
    ('56', 'Pre pensionado con aporte voluntario a salud'),
    ('57', 'Independiente voluntario al sistema de riesgos laborales'),
    ('58', 'Estudiantes de prácticas laborales en el sector público'),
]

sub_tipo_trabajador = [
    ('00', 'No Aplica'),
    ('01', 'Dependiente pensionado por vejez activo'),
]

medio_pago = [
    ('1', 'Instrumento no definido'),
    ('2', 'Crédito ACH'),
    ('3', 'Débito ACH'),
    ('4', 'Reversión débito de demanda ACH'),
    ('5', 'Reversión crédito de demanda ACH'),
    ('6', 'Crédito de demanda ACH'),
    ('7', 'Débito de demanda ACH'),
    ('8', 'Mantener'),
    ('9', 'Clearing Nacional o Regional'),
    ('10', 'Efectivo'),
    ('11', 'Reversión Crédito Ahorro'),
    ('12', 'Reversión Débito Ahorro'),
    ('13', 'Crédito Ahorro'),
    ('14', 'Débito Ahorro'),
    ('15', 'Bookentry Crédito'),
    ('16', 'Bookentry Débito'),
    ('17', 'Concentración de la demanda en efectivo /Desembolso Crédito (CCD)'),
    ('18', 'Concentración de la demanda en efectivo / Desembolso (CCD) débito'),
    ('19', 'Crédito Pago negocio corporativo (CTP)'),
    ('20', 'Cheque'),
    ('21', 'Proyecto bancario'),
    ('22', 'Proyecto bancario certificado'),
    ('23', 'Cheque bancario'),
    ('24', 'Nota cambiaria esperando aceptación'),
    ('25', 'Cheque certificado'),
    ('26', 'Cheque Local'),
    ('27', 'Débito Pago Negocio Corporativo (CTP)'),
    ('28', 'Crédito Negocio Intercambio Corporativo (CTX)'),
    ('29', 'Débito Negocio Intercambio Corporativo (CTX)'),
    ('30', 'Transferencia Crédito'),
    ('31', 'Transferencia Débito'),
    ('32', 'Concentración Efectivo / Desembolso Crédito plus (CCD+)'),
    ('33', 'Concentración Efectivo / Desembolso Débito plus (CCD+)'),
    ('34', 'Pago y depósito pre acordado (PPD)'),
    ('35', 'Concentración efectivo ahorros / Desembolso Crédito (CCD)'),
    ('36', 'Concentración efectivo ahorros / Desembolso Crédito (CCD)'),
    ('37', 'Pago Negocio Corporativo Ahorros Crédito (CTP)'),
    ('38', 'Pago Negocio Corporativo Ahorros Débito (CTP)'),
    ('39', 'Crédito Negocio Intercambio Corporativo (CTX)'),
    ('40', 'Débito Negocio Intercambio Corporativo (CTX)'),
    ('41', 'Concentración efectivo/Desembolso Crédito plus (CCD+)'),
    ('42', 'Consignación bancaria'),
    ('43', 'Concentración efectivo / Desembolso Débito plus (CCD+)'),
    ('44', 'Nota cambiaria'),
    ('45', 'Transferencia Crédito Bancario'),
    ('46', 'Transferencia Débito Interbancario'),
    ('47', 'Transferencia Débito Bancaria'),
    ('48', 'Tarjeta Crédito'),
    ('49', 'Tarjeta Débito'),
    ('50', 'Postgiro'),
    ('51', 'Telex estándar bancario francés'),
    ('52', 'Pago comercial urgente'),
    ('53', 'Pago Tesorería Urgente'),
    ('60', 'Nota promisoria'),
    ('61', 'Nota promisoria firmada por el acreedor'),
    ('62', 'Nota promisoria firmada por el acreedor, avalada por el banco'),
    ('63', 'Nota promisoria firmada por el acreedor, avalada por un tercero'),
    ('64', 'Nota promisoria firmada por el banco'),
    ('65', 'Nota promisoria firmada por un banco avalada por otro banco'),
    ('66', 'Nota promisoria firmada'),
    ('67', 'Nota promisoria firmada por un tercero avalada por un banco'),
    ('70', 'Retiro de nota por el por el acreedor'),
    ('71', 'Bonos'),
    ('72', 'Vales'),
    ('74', 'Retiro de nota por el por el acreedor sobre un banco'),
    ('75', 'Retiro de nota por el acreedor, avalada por otro banco'),
    ('76', 'Retiro de nota por el acreedor, sobre un banco avalada por un tercero'),
    ('77', 'Retiro de una nota por el acreedor sobre un tercero'),
    ('78', 'Retiro de una nota por el acreedor sobre un tercero avalada por un banco'),
    ('91', 'Nota bancaria transferible'),
    ('92', 'Cheque local trasferible'),
    ('93', 'Giro referenciado'),
    ('94', 'Giro urgente'),
    ('95', 'Giro formato abierto'),
    ('96', 'Método de pago solicitado no usado'),
    ('97', 'Clearing entre partners'),
    ('98', 'Cuentas de ahorro de tramite simplificado (CATS) (Nequi, Daviplata, etc)'),
    ('zzz', 'Acuerdo mutuo'),
]

class HrContract(models.Model):
    _inherit = 'hr.contract'

    porcentaje_comision = fields.Float(string='% Comisión')
    lunch_hours = fields.Float(string='Horas de comida', default=1)
    minute_delay_allowed = fields.Integer(string='Minutos de retraso permitidos', default=0)
    # schedule_pay = fields.Selection(selection_add=[('day', 'Diario')])
    # fecha_pago_cesantias = fields.Date(string='Fecha pago cesantias', required=True)
    tipo_trabajador_ti = fields.Selection(tipo_trabajador, string='Tipo de trabajador', index=True, default=tipo_trabajador[0][0])
    cuenta_debito = fields.Integer(string='Debito', default=0)
    cuenta_credito = fields.Integer(string='Credito', default=0)
    sub_tipo_trabajador_ti = fields.Selection(sub_tipo_trabajador, string='Subtipo de trabajador', index=True, default=sub_tipo_trabajador[0][0])
    medio_de_pagos = fields.Selection(medio_pago, string='Medio de pago', index=True, default=medio_pago[0][0])
    # Prestaciones Sociales
    cesantias = fields.Float(string='Cesantias')
    interes_sobre_cesantias = fields.Float(string='Interes S/Cesantias')
    prima_no1 = fields.Float(string='Prima 1')
    prima_no2 = fields.Float(string='Prima 2')
    vacaciones = fields.Float(string='Vacaciones')
    # Adicionales salario
    salario_integral = fields.Boolean(string='Integral', compute='_compute_habilitar_salario_integral')
    pagar_auxilio_transporte = fields.Boolean(string='Pagar Aux. Transp.', compute='_compute_pagar_auxilio_transporte')
    actividad_alto_riesgo = fields.Boolean(string='Activ. Alto Riesgo', default=False)
    # XML
    contract_type = fields.Selection([
        ('1', 'Término fijo'),
        ('2', 'Término Indefinido'),
        ('3', 'Obra o Labor'),
        ('4', 'Aprendizaje'),
        ('5', 'Prácticas o Pasantías'),
    ], string='Tipo de Contrato')
    # Pagar Horas extras
    allow_extra_hours = fields.Boolean(string='Permitir horas extras', default=False, compute='_compute_pagar_horas_extras')
    pay_dominical = fields.Boolean(string='Pagar dominical', default=False, help="Si no se habilita, no se pagarían dominical, se compensaria por un día de descanso")
    # Override field
    schedule_pay = fields.Selection([
        ('weekly', 'Semanal'),
        ('decenal', 'Decenal'),
        # ('catorcenal', 'Catorcenal'),
        ('bi-weekly', 'Quincenal'),
        ('monthly', 'Mensual'),
        ('otro', 'Otro'),
        #('days', 'Dias'),
    ], string='Pago planificado')
    contract_schedule_pay = fields.Selection([
        ('weekly', 'Semanal'),
        ('decenal', 'Decenal'),
        # ('catorcenal', 'Catorcenal'),
        ('bi-weekly', 'Quincenal'),
        ('monthly', 'Mensual'),
        ('otro', 'Otro'),
        #('days', 'Dias'),
    ], string='Pago Planificado Según Contrato')
    ultimas_vacaciones_asiganadas = fields.Date(string='Vacaciones asignadas', help="Última fecha de vacaciones activadas!", readonly=False)
    pagar_vacaciones = fields.Boolean(string='Pagar vacaciones', default=False, readonly=False)
    vacaciones_acumuladas = fields.Integer(string='Vacaciones acumuladas', readonly=False)
    fecha_pago_liquidacion = fields.Date(string='Fecha liquidación')
    dias_trabajados = fields.Integer(string='Días a liquidar', compute='calcular_dias_trabajados')
    dias_vacaciones = fields.Float(string='Dias vacaciones', compute='calcular_dias_vacaciones')
    causa_liquidacion = fields.Selection([
        ('renuncia', 'Renuncia'),
        ('despido', 'Despido')
    ], string='Causa Liquidación', default='despido')
    liquidacion_justa_causa = fields.Selection([
        ('si', 'Si'),
        ('no', 'No'),
    ], string='¿Liquidación justa causa?', default='si')
    # BASES LIQUIDACION
    salario_promedio = fields.Float(string='Salario base', readonly=True)
    base_vacaciones = fields.Float(string='Base vacaciones', readonly=True)
    base_cesantias = fields.Float(string='Base cesantías', readonly=True)
    base_prima = fields.Float(string='Base prima', readonly=True)
    liquidacion = fields.Boolean(string='Liquidación', default=False)
    # Resumen Liquidacion
    liq_vacaciones = fields.Float(string='Vacaciones', readonly=True)
    liq_cesantias = fields.Float(string='Cesantías', readonly=True)
    liq_intereses_cesantias = fields.Float(string='Intereses Cesantías', readonly=True)
    liq_prima = fields.Float(string='Prima', readonly=True)
    liq_otros = fields.Float(string='Otros', readonly=True)
    liq_total = fields.Float(string='Total Liquidación', readonly=True)
    # AFILIACIONES
    eps_id = fields.Many2one('res.partner', 'EPS')
    fp_id = fields.Many2one('res.partner', 'Fondo de Pensiones')
    fc_id =fields.Many2one('res.partner', 'Fondo de Cesantías')
    caja_compensacion_id =fields.Many2one('res.partner', 'Caja de Compensación')
    
    
    def liquidar_empleado(self):
        self.struct_id = self.env.user.company_id.estructura_liquidacion_id
    
    @api.onchange('date_start', 'date_end', 'dias_trabajados')
    def calcular_dias_vacaciones(self):
        for item in self:
            leave_ids = self.env['hr.leave'].search([
                ('request_date_from', '<=', item.date_end),
                ('request_date_from', '>=', item.date_start),
                ('state', '=', 'validate'),
                ('employee_id', '=', item.employee_id.id),
                ('holiday_status_id.leave_type', '=', 'otro')
            ])
            dias_ausencias = 0
            for leave in leave_ids:
                dias_ausencias += (leave.request_date_to - leave.request_date_from).days + 1
            item.dias_vacaciones = (item.dias_trabajados - dias_ausencias) * 15 / 360
    
    @api.onchange('date_start', 'date_end')
    def calcular_dias_trabajados(self):
        for item in self:
            if item.date_end:
                item.dias_trabajados = 0
                control_date = item.date_start
                while True:
                    last_day_month = datetime(control_date.year, control_date.month, 1) + relativedelta(months=1, days=-1)
                    if last_day_month.date() <= item.date_end:
                        if control_date.day > 1:
                            item.dias_trabajados += 30 - control_date.day + 1
                        else:
                            item.dias_trabajados += 31 - control_date.day
                        if last_day_month.date() == item.date_end:
                            break
                    else:
                        item.dias_trabajados += (item.date_end.day - control_date.day) + 1
                        break
                    control_date = control_date - relativedelta(days=control_date.day - 1) + relativedelta(months=1)
    
    def buscar_vacaciones(self):
        employee_ids = self.env['hr.employee'].search([('active', '=', True),('id', '=', 21)])
        for employee in employee_ids:
            if employee.contract_id:
                contract_id = employee.contract_id
                if contract_id.ultimas_vacaciones_asiganadas:
                    if (contract_id.ultimas_vacaciones_asiganadas + relativedelta(years=1)) == date.today():
                        contract_id.ultimas_vacaciones_asiganadas = date.today()
                        contract_id.pagar_vacaciones = True
                        contract_id.vacaciones_acumuladas += 15
                        self._crear_asignacion_vacaciones(employee)
                else:
                    if (contract_id.date_start + relativedelta(years=1)) == date.today():
                        contract_id.ultimas_vacaciones_asiganadas = date.today()
                        contract_id.pagar_vacaciones = True
                        contract_id.vacaciones_acumuladas += 15
                        self._crear_asignacion_vacaciones(employee)

    def _crear_asignacion_vacaciones(self, employee):
        # Crear asignación de vacaciones
        allocation_id = self.env['hr.leave.allocation'].create({
            'name': 'Vacaciones %s' % date.today().year,
            'holiday_status_id': self.env['hr.leave.type'].search([('leave_type', '=', 'vacaciones')]).id,
            'number_of_days_display': 15,
            'number_of_days': 15,
            'employee_id': employee.id
        })
        if allocation_id:
            allocation_id.action_approve()
    
    # Metodos de apoyo
    @api.depends('wage')
    def _compute_pagar_auxilio_transporte(self):
        if self.wage <= float(self.env.user.company_id.smmlv_value) * 2:
            self.pagar_auxilio_transporte = True
        else:
            self.pagar_auxilio_transporte = False
    
    @api.depends('wage')
    def _compute_habilitar_salario_integral(self):
        if self.wage >= float(self.env.user.company_id.smmlv_value) * 13:
            self.salario_integral = True
        else:
            self.salario_integral = False
    
    @api.depends('employee_id.manager')
    def _compute_pagar_horas_extras(self):
        if self.employee_id.manager:
            self.allow_extra_hours = False
        else:
            self.allow_extra_hours = True

    def _convert_utc_date_to_usertz(self, fecha_utc):
        user_tz = self.env.user.tz
        fmt = '%Y-%m-%d %H:%M:%S'
        local = pytz.timezone(user_tz)
        return str(datetime.strftime(pytz.utc.localize(datetime.strptime(fecha_utc, fmt)).astimezone(local), fmt))

    def convert_TZ_UTC(self, TZ_datetime):
        # Metodo que permite convertir la fecha y hora retornada por el SAT a la fecha y hora correcta para guardar en la base de datos y que coincida con la real
        fmt = "%Y-%m-%d %H:%M:%S"
        # Current time in UTC
        now_utc = datetime.now(pytz.timezone('UTC'))
        # Convert to current user time zone
        now_timezone = now_utc.astimezone(pytz.timezone(self.env.user.tz))
        UTC_OFFSET_TIMEDELTA = datetime.strptime(now_utc.strftime(fmt), fmt) - datetime.strptime(now_timezone.strftime(fmt), fmt)
        local_datetime = datetime.strptime(TZ_datetime, fmt)
        result_utc_datetime = local_datetime + UTC_OFFSET_TIMEDELTA
        return result_utc_datetime.strftime(fmt)
    
    """def total_period_days(self, payslip):
        total_days = (payslip.date_to - payslip.date_from).days + 1
        # Definir cuanto pagar considerando días trabajados y descuento de ausencias
        if payslip.contract_id.schedule_pay == 'weekly':
            return 7
        elif payslip.contract_id.schedule_pay == 'decenal':
            return 10
        elif payslip.contract_id.schedule_pay == 'catorcenal':
            return 14
        elif payslip.contract_id.schedule_pay == 'bi-weekly':
            total_days = total_days if total_days < 15 or total_days > 16 else 15
            return total_days
        elif payslip.contract_id.schedule_pay == 'monthly':
            total_days = total_days if total_days < 28 or total_days > 31 else 30
            return 30
        elif payslip.contract_id.schedule_pay == 'days':
            return (payslip.date_to - payslip.date_from).days + 1"""

    def get_comision(self, payslip):
        sales = self.env['account.invoice'].search([('user_id', '=', payslip.contract_id.employee_id.user_id.id), ('date_invoice', '>=', payslip.date_from), ('date_invoice', '<=', payslip.date_to)])
        if not sales:
            return 0

        total_sales = 0
        for sale in sales:
            total_sales += sale.amount_total
        return total_sales * (payslip.contract_id.porcentaje_comision / 100)
    
    def calcular_dias_trabajados_manual(self, payslip):
        total_days = (payslip.date_to - payslip.date_from).days + 1
        if payslip.contract_id.schedule_pay == 'monthly':
            if payslip.date_from.day > 1 and payslip.date_to.day == 31: # Inicio trabajo despues del 1
                total_days = (payslip.date_to - payslip.date_from).days
            elif payslip.date_from.day > 1 and payslip.date_to.day < 31:
                total_days = total_days
            elif payslip.date_from.day == 1 and payslip.date_to.day <= 30 and payslip.date_to.month != 2:
                total_days = total_days
            elif payslip.date_from.day == 1 and (payslip.date_to.day == 28 or payslip.date_to.day == 29) and payslip.date_to.month == 2:
                total_days = 30
            elif payslip.date_from.day == 1 and payslip.date_to.day == 31:
                total_days = 30
            elif payslip.date_from.day == 1 and payslip.date_to.day < 28:
                total_days = total_days
        elif payslip.contract_id.schedule_pay == 'bi-weekly':
            if payslip.date_from.day > 1 and payslip.date_to.day == 15: # Inicio trabajo despues del 1
                total_days = (payslip.date_to - payslip.date_from).days + 1
            elif payslip.date_from.day > 1 and payslip.date_to.day < 15:
                total_days = total_days
            elif payslip.date_from.day == 1 and (payslip.date_to.day == 15 or payslip.date_to.day < 15):
                total_days = total_days
            elif payslip.date_from.day > 16 and payslip.date_to.day == 31:
                total_days = (payslip.date_to - payslip.date_from).days
            elif payslip.date_from.day > 16 and payslip.date_to.day < 31:
                total_days = total_days
            elif payslip.date_from.day == 16 and payslip.date_to.day <=  30 and payslip.date_to.month != 2:
                total_days = total_days
            elif payslip.date_from.day == 16 and (payslip.date_to.day == 28 or payslip.date_to.day == 29) and payslip.date_to.month == 2:
                total_days = 15
            elif payslip.date_from.day == 1 and payslip.date_to.day < 28:
                total_days = total_days
            else:
                total_days = total_days if total_days < 15 or total_days > 16 else 15
        elif payslip.contract_id.schedule_pay == 'weekly':
            total_days = 6
            if payslip.contract_id.pay_dominical:
                total_days += 1
        elif payslip.contract_id.schedule_pay == 'decenal':
            total_days = 10
        elif payslip.contract_id.schedule_pay == 'catorcenal':
            total_days = 14
        return total_days
    
    def _get_dias_trabajados(self, contract, payslip):
        manual_payroll = self.env['ir.config_parameter'].get_param('hr_payroll_colombia.manual_payroll')
        if manual_payroll:
            total_days = self.calcular_dias_trabajados_manual(payslip)
            
            dias_ausencia = self.get_all_leaves(payslip, payslip.employee_id.id)
            return total_days - (dias_ausencia[0] + dias_ausencia[1])
    
    def get_dias_trabajados_con_vacaciones(self, payslip):
        manual_payroll = self.env['ir.config_parameter'].get_param('hr_payroll_colombia.manual_payroll')
        if manual_payroll:
            total_days = self.calcular_dias_trabajados_manual(payslip)
            
            dias_ausencia = self.get_all_leaves(payslip, payslip.employee_id.id)
            return total_days - (dias_ausencia[1] + dias_ausencia[3])
    
    def _get_ausencias(self, payslip, employee):
        # Consultar ausencias que iniciean antes o al inicio de fecha de nomina
        ausencias = payslip.env['hr.leave'].search([
            ('request_date_from', '<=', payslip.date_from), 
            ('request_date_to', '>=', payslip.date_from), 
            ('employee_id', '=', int(employee)), 
            ('state', '=', 'validate'),])
        
        # Ausencias donde sus fechas están dentro o igual del periodo de nomina
        ausencias |= payslip.env['hr.leave'].search([
            ('request_date_from', '>=', payslip.date_from),
            ('request_date_to', '<=', payslip.date_to),
            ('employee_id', '=', int(employee)), 
            ('state', '=', 'validate')])
        
        # Ausencias que inician al final o igual de fecha final de nomina, y terminan en la siguiente nomina
        ausencias |= payslip.env['hr.leave'].search([
            ('request_date_from', '<=', payslip.date_to),
            ('request_date_to', '>=', payslip.date_to),
            ('employee_id', '=', int(employee)),
            ('state', '=', 'validate')])

        return ausencias
    
    def _cosultar_dia_festivo(self, payslip, date):
        dia_feriado = self.env['hr.holidays.calendar'].search([('day', '=', date.day), ('month', '=', date.month)])
        if dia_feriado:
            return True
        return False
        #if not payslip.employee_id:
        #    raise ValidationError('Defina un calendario de días festivo para el empleado: %s' % payslip.employee_id.name)
        
        # Employee = self.env['hr.employee'].search([('id', '=', int(payslip.employee_id))])
        #if Employee.holidays_calendar_id.holiday_calendar_line_ids.filtered(lambda x: x.date == date):
        #    return True
        #return False
        
    def _descontar_domingo_auxilio(self, payslip, ausencia, semanas_ausencia, semanas, date_to, date_from):
        # fecha_control = payslip.date_from
        fecha_control = date_from
        for n in range(0, (date_to - date_from).days + 1):
            for semana in semanas.items():
                # Consultar si es dia festivo
                dia_festivo = self._cosultar_dia_festivo(payslip, fecha_control)
                
                if not dia_festivo:
                    if fecha_control in semana[1] and not semanas_ausencia[semana[0]] and ausencia.holiday_status_id.leave_type == 'otro':
                        semanas_ausencia[semana[0]] = True
            fecha_control += timedelta(days=1)
        return semanas_ausencia
    
    def _descontar_domingo_ausencia_por_luto(self, payslip, date_to, date_from):
        fecha_control = payslip.date_from
        domingos_festivos = 0
        
        for n in range(0, (date_to - date_from).days + 1):
            # Consultar si es dia festivo
            dia_festivo = self._cosultar_dia_festivo(payslip, fecha_control)
            
            if dia_festivo:
                domingos_festivos += 1
            fecha_control += timedelta(days=1)
        return domingos_festivos
        
    
    def get_minutos_trabajados(self, contract, payslip):
        manual_payroll = self.env['ir.config_parameter'].get_param('hr_payroll_colombia.manual_payroll')
        if not manual_payroll:
            fecha_inicial = payslip.date_from
            fecha_final = payslip.date_to

            fecha_control = fecha_inicial
            dias_fin_de_semana = 0

            for count in range(1, (fecha_final - fecha_inicial).days + 2):
                dayweek_control = datetime.weekday(fecha_control)
                
                if (dayweek_control == 5 or dayweek_control == 6) and not contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == dayweek_control):
                    dias_fin_de_semana += 1
                fecha_control += timedelta(days=1)
            
            #Determinar forma de pago:
            # Si es mensual, considerar cuantos días tiene el mes, si tiene menos de 30, sumas para completarlos, si tiene 31 restar uno
            month_days = monthrange(fecha_inicial.year, fecha_inicial.month)[1]
            if contract.schedule_pay == 'monthly':
                if month_days == 31:
                    dias_fin_de_semana -= 1
                elif month_days == 28:
                    dias_fin_de_semana += 2
                elif month_days == 29:
                    dias_fin_de_semana += 1
            elif contract.schedule_pay == 'bi-weekly':
                if fecha_inicial.day == 16:
                    if month_days == 31:
                        dias_fin_de_semana -= 1
                    elif month_days == 28:
                        dias_fin_de_semana += 2
                    elif month_days == 29:
                        dias_fin_de_semana += 1
            
            minute_pay =  contract.wage / 30 / 480

            # Consultamos todos los registros de asistencia del empleado
            attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', contract.employee_id.id), 
                ('check_in', '>=', payslip.date_from), ('check_out', '<=', payslip.date_to)])
            
            # Consultar los minutos permitidos para llegar después de la hora de entrada
            minute_delay = contract.minute_delay_allowed
            # total_minutes = 0
            
            total_diurno = 0
            total_nocturno = 0
            totol_minutos_pagados = 0
            for attendance_id in attendance_ids:
                # Consultar el día de la semana de la asistencia en base a la hora de entrada
                attendance_day = datetime.weekday(attendance_id.check_in)

                # Consultar el horario de entrada del dia correspondiente a la asistencia
                calendar_day = contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == attendance_day)
                
                if not calendar_day:
                    continue
                
                # Sumar los minutos permitidos de tolerancia a la hora de entrada
                entrada = str(attendance_id.check_in).split(" ")[0] + ' ' + str(timedelta(hours=calendar_day.hour_from))
                salida = str(attendance_id.check_out).split(' ')[0] + ' ' + str(timedelta(hours=calendar_day.hour_to))
                tiempo_comida = timedelta(hours=contract.lunch_hours)

                minutos_tolerancia = timedelta(minutes=contract.minute_delay_allowed)
                hour_from_with_delay = datetime.strptime(entrada, '%Y-%m-%d %H:%M:%S') + minutos_tolerancia

                # Determinar la entrada
                check_in_system = self._convert_utc_date_to_usertz(str(attendance_id.check_in))
                check_in_system = datetime.strptime(check_in_system, '%Y-%m-%d %H:%M:%S')
                if check_in_system <= hour_from_with_delay:
                    check_in = datetime.strptime(entrada, '%Y-%m-%d %H:%M:%S')
                else:
                    check_in = check_in_system
                
                # Determinar la salida
                check_out_system = self._convert_utc_date_to_usertz(str(attendance_id.check_out))
                check_out_system = datetime.strptime(check_out_system, '%Y-%m-%d %H:%M:%S')
                if check_out_system <= datetime.strptime(salida, '%Y-%m-%d %H:%M:%S'):
                    check_out = check_out_system
                else:
                    check_out = datetime.strptime(salida, '%Y-%m-%d %H:%M:%S')
                
                tiempo_trabajado = check_out - check_in - tiempo_comida
                tiempo_trabajado_minutos = round(tiempo_trabajado.seconds / 60)
                if calendar_day.day_period == 'morning':
                    total_diurno += round(tiempo_trabajado_minutos, 0) * minute_pay
                else:
                    total_nocturno += round(tiempo_trabajado_minutos, 0) * (minute_pay * 1.35)
                totol_minutos_pagados += tiempo_trabajado_minutos
            
            fin_semana = dias_fin_de_semana * 480 * minute_pay
            return round(total_nocturno + total_diurno + fin_semana)
        else:
            day_pay =  contract.wage / 30
            total_days = self.calcular_dias_trabajados_manual(payslip)
            
            dias_ausencia = self.get_all_leaves(payslip, payslip.employee_id)
            
            dias_a_pagar = total_days - dias_ausencia[0] - dias_ausencia[1]
            
            # Descontar los días festivos, siempre y cuando no coincida con una ausencia
            dias_a_pagar -= self._descontar_dias_festivos_fuera_de_ausencia(payslip)
            
            # Sumar domingos que coinciden dentro del rango de vacaciones
            vacaciones = self._get_ausencias(payslip, payslip.employee_id)
            no_laborables_en_vacaciones = 0
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type == 'vacaciones':
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if vac.request_date_from <= payslip.date_from and vac.request_date_to <= payslip.date_to:            
                        no_laborables_en_vacaciones += self._sumar_domingos_rango_vacaciones(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to <= payslip.date_to:
                        no_laborables_en_vacaciones += self._sumar_domingos_rango_vacaciones(payslip, vac.request_date_to, vac.request_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        no_laborables_en_vacaciones += self._sumar_domingos_rango_vacaciones(payslip, payslip.date_to, vac.request_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.request_date_from <= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        no_laborables_en_vacaciones += self._sumar_domingos_rango_vacaciones(payslip, payslip.date_to, payslip.date_from)
            
            return (dias_a_pagar + no_laborables_en_vacaciones) * day_pay
    
    def _sumar_domingos_rango_vacaciones(self, payslip, date_to, date_from):
        fecha_control = date_from
        no_laborables = 0
        
        for n in range(0, (date_to - date_from).days + 1):
            day_of_week = datetime.weekday(fecha_control)
            calendar_day = payslip.dict.contract_id.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == day_of_week)
                
            if not calendar_day:
                no_laborables += 1
            fecha_control += timedelta(days=1)
        return no_laborables
            
    def _descontar_dias_festivos_fuera_de_ausencia(self, payslip):
        fecha_control = payslip.date_from
        ausencias = self._get_ausencias(payslip, payslip.employee_id)
        dias_descontar = 0
        
        for i in range(0, (payslip.date_to - payslip.date_from).days + 1):
            # Si es un dia festivo, validamos si coincide con una ausencia, para no descontar
            if self._cosultar_dia_festivo(payslip, fecha_control):
                if ausencias:
                    for ausencia in ausencias:
                        if fecha_control >= ausencia.request_date_from and fecha_control <= ausencia.request_date_to:
                            dias_descontar = dias_descontar
                        else:
                            dias_descontar += 1
                    
                else:
                    dias_descontar += 1
            fecha_control += timedelta(days=1)
        return dias_descontar
        
    """def get_tipo_horas_extra(self, contract, payslip, type):
        # TODO: Agregar campo en contrato para permitir el pago o contabilización de horas extras
        # TODO: Agregar campo en contrato para permitir pago de hora adicional por petición del jefe.
        # TODO: Considerar horas adicionales si entra más temprano pero solo por petición del jefe.
        fecha_inicial = payslip.date_from
        fecha_final = payslip.date_to

        # Consultamos todos los registros de asistencia del empleado
        attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', contract.employee_id.id), 
            ('check_in', '>=', payslip.date_from), ('check_out', '<=', payslip.date_to)])
        
        # Consultar los minutos permitidos para llegar después de la hora de entrada
        # minute_delay = contract.minute_delay_allowed
        # total_minutes = 0
        
        total_diurno = 0
        total_nocturno = 0
        totol_minutos_pagados = 0
        for attendance_id in attendance_ids:
            # Consultar el día de la semana de la asistencia en base a la hora de entrada
            attendance_day = datetime.weekday(attendance_id.check_in)

            # Consultar el horario de entrada del dia correspondiente a la asistencia
            calendar_day = contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == attendance_day)
            
            if not calendar_day:
                continue

            entrada = str(attendance_id.check_in).split(" ")[0] + ' ' + str(timedelta(hours=calendar_day.hour_from))
            salida = str(attendance_id.check_out).split(' ')[0] + ' ' + str(timedelta(hours=calendar_day.hour_to))

            # Determinar la entrada
            check_in_system = self._convert_utc_date_to_usertz(str(attendance_id.check_in))
            check_in_system = datetime.strptime(check_in_system, '%Y-%m-%d %H:%M:%S')
            if check_in_system <= hour_from_with_delay:
                check_in = datetime.strptime(entrada, '%Y-%m-%d %H:%M:%S')
            else:
                check_in = check_in_system
            
            # Determinar la salida
            check_out_system = self._convert_utc_date_to_usertz(str(attendance_id.check_out))
            check_out_system = datetime.strptime(check_out_system, '%Y-%m-%d %H:%M:%S')
            if check_out_system <= datetime.strptime(salida, '%Y-%m-%d %H:%M:%S'):
                check_out = check_out_system
            else:
                check_out = datetime.strptime(salida, '%Y-%m-%d %H:%M:%S')
            
            tiempo_trabajado = check_out - check_in - tiempo_comida
            tiempo_trabajado_minutos = round(tiempo_trabajado.seconds / 60)
            if calendar_day.day_period == 'morning':
                total_diurno += round(tiempo_trabajado_minutos, 0) * minute_pay
            else:
                total_nocturno += round(tiempo_trabajado_minutos, 0) * (minute_pay * 1.35)
            totol_minutos_pagados += tiempo_trabajado_minutos
        
        fin_semana = dias_fin_de_semana * 480 * minute_pay
        return round(total_nocturno + total_diurno + fin_semana)"""
    
    def get_seconds(self, contract, week_day, date_from, date_to):
        calendar_day = contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == week_day)
        calendar_hour_in = str(date_from).split(' ')[0] + ' ' + str(timedelta(hours=calendar_day.hour_from))
        calendar_hour_out = str(date_to).split(' ')[0] + ' ' + str(timedelta(hours=calendar_day.hour_to))
        lunch_time = timedelta(hours=contract.lunch_hours)
        return datetime.strptime(calendar_hour_out, '%Y-%m-%d %H:%M:%S') - datetime.strptime(calendar_hour_in, '%Y-%m-%d %H:%M:%S') - lunch_time
    
    def ausencia_no_remunerada_minutos(self, contract, ausencias):
        discount = 0

        for item in ausencias:
            if item.holiday_status_id.unpaid == True:

                # Ausencia por medio dia
                if item.request_unit_half == True:
                    week_day = datetime.weekday(item.request_date_from)
                    seconds = self.get_seconds(contract, week_day, item.request_date_from, item.request_date_to)
                    discount += ((seconds.seconds / 60) / 2) * (contract.wage / 14400)

                # Ausencia por dias
                elif item.holiday_status_id.request_unit == 'day':
                    # Consultar por los dias de ausencia las horas de trabajo por cada dia
                    current_day = item.request_date_from
                    for dia in range(1, int(item.number_of_days_display) + 1, 1):
                        week_day = datetime.weekday(current_day)
                        seconds = self.get_seconds(contract, week_day, item.request_date_from, item.request_date_to)
                        discount += (seconds.seconds / 60) * (contract.wage / 14400)
                        current_day = current_day + timedelta(days=1)
                
                # Ausencia por horas
                elif item.holiday_status_id.request_unit_hours == True:
                    return True
        return discount
    
    def consultar_ausencia_remunerada(self, contract, payslip, employee):
        ausencias = self._get_ausencias(payslip, employee)
        
        if ausencias:
            dias = 0
            for ausencia in ausencias:
                if ausencia.holiday_status_id.leave_type == 'ausencia_remunerada':
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:            
                        dias += (ausencia.request_date_to - payslip.date_from).days + 1
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                        dias += (ausencia.request_date_to - ausencia.request_date_from).days + 1
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - ausencia.request_date_from).days + 1
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1

            pago_x_dia = contract.wage / 30

            return round(pago_x_dia * dias, 0)
        else:
            return 0
    
    def consultar_dias_festivos(self, payslip):
        fecha_control = payslip.date_from
        dias_festivos = 0
        
        for n in range(0, (payslip.date_to - payslip.date_from).days + 1):
            if self._cosultar_dia_festivo(payslip, fecha_control):
                dias_festivos += 1
                
            fecha_control += timedelta(days=1)
        
        pago_x_dia = payslip.contract_id.wage / 30

        return round(pago_x_dia * dias_festivos, 0)
    
    def get_arl(self, payslip, employee):
        ausencias = self._get_ausencias(payslip, employee)

        if ausencias:
            dias = 0
            for ausencia in ausencias:
                if ausencia.holiday_status_id.leave_type == 'incapacidad_arl':
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:            
                        dias += (ausencia.request_date_to - payslip.date_from).days + 1
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                        dias += (ausencia.request_date_to - ausencia.request_date_from).days + 1
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - ausencia.request_date_from).days + 1
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1

            #pago_x_dia = contract.wage / 30

            #return round(pago_x_dia * dias, 0)
            return dias
        else:
            return 0
        
    def get_all_leaves(self, payslip, employee):
        ausencias = self._get_ausencias(payslip, employee)
        
        # Definir los días de cada semana de acuerdo al rago de la nómina
        no_semana = 1
        semanas = {}
        semanas_ausencia = {}
        fecha_control = payslip.date_from
        dias_festivos = 0
        
        for i in range(0, (payslip.date_to - payslip.date_from).days + 1):
            dayweek_control = datetime.weekday(fecha_control)
            if dayweek_control == 6:
                if no_semana in semanas:
                    semanas[no_semana].append(fecha_control)
                else:
                    semanas[no_semana] = [fecha_control]
                no_semana += 1
            else:
                if no_semana in semanas:
                    semanas[no_semana].append(fecha_control)
                else:
                    semanas[no_semana] = [fecha_control]
            semanas_ausencia[no_semana] = False
            
            # Validar si hay días festivos, para descontarlos se pagan en regla ausencias remuneradas
            if self._cosultar_dia_festivo(payslip, fecha_control):
                dias_festivos += 1
            
            fecha_control += timedelta(days=1)

        # [Ausencias justificas, Ausencias no justificadas, dias festivos, festivos a descontar adicional]
        dias = [0, 0, 0, 0]
        if ausencias:
            for ausencia in ausencias:
                # Si la ausencia inicia antes del inicio de nomina y termina antes de de la fecha final
                if ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    if ausencia.holiday_status_id.leave_type != 'otro':
                        dias[0] += (ausencia.request_date_to - payslip.date_from).days + 1
                    else:
                        if ausencia.request_unit_half:
                            dias[1] += 0.5
                        else:
                            dias[1] += (ausencia.request_date_to - payslip.date_from).days + 1
                    # Si la ausencia es no justificada descontar domingo
                    self._descontar_domingo_auxilio(payslip, ausencia, semanas_ausencia, semanas, ausencia.request_date_to, payslip.date_from)
                # Si la ausencia esta dentro del periodo de la nomina
                elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    if ausencia.holiday_status_id.leave_type != 'otro':
                        dias[0] += (ausencia.request_date_to - ausencia.request_date_from).days + 1
                    else:
                        if ausencia.request_unit_half:
                            dias[1] += 0.5
                        else:
                            dias[1] += (ausencia.request_date_to - ausencia.request_date_from).days + 1
                    # Si la ausencia es no justificada descontar domingo
                    self._descontar_domingo_auxilio(payslip, ausencia, semanas_ausencia, semanas, ausencia.request_date_to, ausencia.request_date_from)
                # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    if ausencia.holiday_status_id.leave_type != 'otro':
                        dias[0] += (payslip.date_to - ausencia.request_date_from).days + 1
                    else:
                        if ausencia.request_unit_half:
                            dias[1] += 0.5
                        else:
                            dias[1] += (payslip.date_to - ausencia.request_date_from).days + 1
                    # Si la ausencia es no justificada descontar domingo
                    self._descontar_domingo_auxilio(payslip, ausencia, semanas_ausencia, semanas, payslip.date_to, ausencia.request_date_from)
                # Si la ausencia inicia antes o termina despues del periodo de la nomina
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    if ausencia.holiday_status_id.leave_type != 'otro':
                        dias[0] += (payslip.date_to - payslip.date_from).days + 1
                    else:
                        if ausencia.request_unit_half:
                            dias[1] += 0.5
                        else:
                            dias[1] += (payslip.date_to - payslip.date_from).days + 1
                    # Si la ausencia es no justificada descontar domingo
                    self._descontar_domingo_auxilio(payslip, ausencia, semanas_ausencia, semanas, payslip.date_to, payslip.date_from)
        
        # Validar si descontar domingos
        for semana in semanas_ausencia.items():
            if semana[1]:
                dias[1] += 1
        
        # Sumar días festivos
        dias[2] += dias_festivos
        return dias
    
    def calcular_incapacidad_comun(self, payslip, contract, employee):
        # result = payslip.env['hr.contract'].dias_ausencia_eps(payslip)
        ausencias_ids = self._get_ausencias(payslip, employee)
 
        # Ausencias solo por enfermedad
        ausencias = ausencias_ids.filtered(lambda x: x.holiday_status_id.leave_type == 'incapacidad_eps')
        total = 0

        if ausencias:
            dias_ausencias = 0
            for ausencia in ausencias:
                # Ausencia de toda la fecha de nomina, e iniciando en una y cruzandose a otra nomina
                if ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - ausencia.request_date_from + timedelta(days=1)).days
                    no_dia_ausencia = 1
                # Ausencia antes o al inicio de fecha de la nomina y, mayor o igual a fecha final de nomina
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - payslip.date_from + timedelta(days=1)).days
                    no_dia_ausencia = ((ausencia.request_date_to - ausencia.request_date_from) - (payslip.date_to - payslip.date_from)).days
                # Ausencia entre o igual a fechas de nomina
                elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - ausencia.request_date_from + timedelta(days=1)).days
                    no_dia_ausencia = (ausencia.request_date_to - ausencia.request_date_from + timedelta(days=1)).days
                # Ausencia en la nomina anterior y en la nomina actual
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - payslip.date_from + timedelta(days=1)).days
                    no_dia_ausencia = ((ausencia.request_date_to - ausencia.request_date_from) - (ausencia.request_date_to - payslip.date_from) + timedelta(days=1)).days

            valor_x_dia_sm = self.env['hr.salary.rule'].search([('code', '=', 'SMLV_Parametro')])[0].amount_fix / 30
            pago_x_dia = (contract.wage / 30) * 0.6667

            if pago_x_dia < valor_x_dia_sm:
                pago_x_dia = valor_x_dia_sm

            # Saber que dia se le esta pagando
            if no_dia_ausencia <= 90:
                total = pago_x_dia * dias_ausencias
            elif no_dia_ausencia > 90 and no_dia_ausencia <= 180:
                total = pago_x_dia * dias_ausencias * 0.5


            #porcentaje90 = 1 if (pago_x_dia * 0.6667) <= (SMLV_Parametro / 30) else 0.6667
            #porcentaje91 = 1 if (pago_x_dia * 0.5) <= (SMLV_Parametro / 30) else 0.5

            # Pago de 1 a 90 dias
            #dias_pagar = 0
            #if no_dia_ausencia <= 90:
            #    dias_pagar = dias
            #else:
            #    dias_pagar = 90

            #subtotal = (dias_pagar * pago_x_dia * porcentaje90)

            # Pago del dia 91 al 180
            #dias_pagar = 0
            #if dias > 90 and dias <= 180:
            #    if dias <= 180:
            #        dias_pagar = dias - 90
            #    else:
            #        dias_pagar = 90

            #subtotal += dias_pagar * pago_x_dia * porcentaje91

        return total
    
    def calcular_incapacidad_comun_dias(self, payslip, employee):
        # result = payslip.env['hr.contract'].dias_ausencia_eps(payslip)
        ausencias_ids = self._get_ausencias(payslip, employee)
 
        # Ausencias solo por enfermedad
        ausencias = ausencias_ids.filtered(lambda x: x.holiday_status_id.leave_type == 'incapacidad_eps')

        if ausencias:
            dias_ausencias = 0
            for ausencia in ausencias:
                # Ausencia de toda la fecha de nomina, e iniciando en una y cruzandose a otra nomina
                if ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - ausencia.request_date_from + timedelta(days=1)).days
                # Ausencia antes o al inicio de fecha de la nomina y, mayor o igual a fecha final de nomina
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - payslip.date_from + timedelta(days=1)).days
                # Ausencia entre o igual a fechas de nomina
                elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - ausencia.request_date_from + timedelta(days=1)).days
                # Ausencia en la nomina anterior y en la nomina actual
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - payslip.date_from + timedelta(days=1)).days
            return dias_ausencias
        return 0    

    def calcular_ausencia_por_luto(self, payslip, contract, employee):
        ausencias_ids = self._get_ausencias(payslip, employee)
 
        # Ausencias solo por enfermedad
        ausencias = ausencias_ids.filtered(lambda x: x.holiday_status_id.leave_type == 'por_luto')
        total = 0

        if ausencias:
            dias_ausencias = 0
            for ausencia in ausencias:
                # Ausencia de toda la fecha de nomina, e iniciando en una y cruzandose a otra nomina
                if ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - ausencia.request_date_from + timedelta(days=1)).days
                    dias_ausencias -= self._descontar_domingo_ausencia_por_luto(payslip, payslip.date_to, ausencia.request_date_from)
                    no_dia_ausencia = 1
                # Ausencia antes o al inicio de fecha de la nomina y, mayor o igual a fecha final de nomina
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to >= payslip.date_to:
                    dias_ausencias += (payslip.date_to - payslip.date_from + timedelta(days=1)).days
                    dias_ausencias -= self._descontar_domingo_ausencia_por_luto(payslip, payslip.date_to, payslip.date_from)
                    no_dia_ausencia = ((ausencia.request_date_to - ausencia.request_date_from) - (payslip.date_to - payslip.date_from)).days
                # Ausencia entre o igual a fechas de nomina
                elif ausencia.request_date_from >= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - ausencia.request_date_from + timedelta(days=1)).days
                    dias_ausencias -= self._descontar_domingo_ausencia_por_luto(payslip, ausencia.request_date_to, ausencia.request_date_from)
                    no_dia_ausencia = (ausencia.request_date_to - ausencia.request_date_from + timedelta(days=1)).days
                # Ausencia en la nomina anterior y en la nomina actual
                elif ausencia.request_date_from <= payslip.date_from and ausencia.request_date_to <= payslip.date_to:
                    dias_ausencias += (ausencia.request_date_to - payslip.date_from + timedelta(days=1)).days
                    dias_ausencias -= self._descontar_domingo_ausencia_por_luto(payslip, ausencia.request_date_to, payslip.date_from)
                    no_dia_ausencia = ((ausencia.request_date_to - ausencia.request_date_from) - (ausencia.request_date_to - payslip.date_from) + timedelta(days=1)).days

            # valor_x_dia_sm = self.env['hr.salary.rule'].search([('code', '=', 'SMLV_Parametro')])[0].amount_fix / 30
            # total =  valor_x_dia_sm * dias_ausencias
            total = (contract.wage / 30) * dias_ausencias
        return total
    
    """def dias_ausencia_eps(self, payslip):
        ausencias = self._get_ausencias(payslip, employee)
        #for a3 in ausencias3:
        #    if a3 not in ausencias:
        #        ausencias += a3
        
        # Ausencias solo por enfermedad
        ausencias = ausencias.filtered(lambda x: x.holiday_status_id.leave_type == 'incapacidad_eps')

        # Establecer los días de ausencia
        dias_ausencias = 0
        for ausencia in ausencias:
            # Ausencia de toda la fecha de nomina, e iniciando en uno y cruzandose a otro nomina
            if ausencia.date_from.date() >= payslip.date_from and ausencia.date_to.date() >= payslip.date_to:
                dias_ausencias += (payslip.date_to - ausencia.date_from.date() + timedelta(days=1)).days
            # Ausencia antes o al inicio de fecha de la nomina y, mayor o igual a fecha final de nomina
            elif ausencia.date_from.date() <= payslip.date_from and ausencia.date_to.date() >= payslip.date_to:
                dias_ausencias += (payslip.date_to - payslip.date_from + timedelta(days=1)).days
            # Ausencia entre o igual a fechas de nomina
            elif ausencia.date_from.date() >= payslip.date_from and ausencia.date_to.date() <= payslip.date_to:
                dias_ausencias += (ausencia.date_to.date() - ausencia.date_from.date() + timedelta(days=1)).days
            # Ausencia en la nomina anterior y en la nomina actual
            elif ausencia.date_from.date() <= payslip.date_from and ausencia.date_to.date() <= payslip.date_to:
                dias_ausencias += (ausencia.date_to.date() - payslip.date_from + timedelta(days=1)).days
        return dias_ausencias"""
    
    def get_worked_days(self, payslip, contract):
        manual_payroll = self.env['ir.config_parameter'].get_param('hr_payroll_colombia.manual_payroll')
        if not manual_payroll:
            fecha_inicial = payslip.date_from
            fecha_final = payslip.date_to

            fecha_control = fecha_inicial
            dias_fin_de_semana = 0

            for count in range(1, (fecha_final - fecha_inicial).days + 2):
                dayweek_control = datetime.weekday(fecha_control)
                
                if (dayweek_control == 5 or dayweek_control == 6) and not contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == dayweek_control):
                    dias_fin_de_semana += 1
                fecha_control += timedelta(days=1)
            
            #Determinar forma de pago:
            # Si es mensual, considerar cuantos días tiene el mes, si tiene menos de 30, sumas para completarlos, si tiene 31 restar uno
            month_days = monthrange(fecha_inicial.year, fecha_inicial.month)[1]
            if contract.schedule_pay == 'monthly':
                if month_days == 31:
                    dias_fin_de_semana -= 1
                elif month_days == 28:
                    dias_fin_de_semana += 2
                elif month_days == 29:
                    dias_fin_de_semana += 1
            elif contract.schedule_pay == 'bi-weekly':
                if fecha_inicial.day == 16:
                    if month_days == 31:
                        dias_fin_de_semana -= 1
                    elif month_days == 28:
                        dias_fin_de_semana += 2
                    elif month_days == 29:
                        dias_fin_de_semana += 1

            # Consultamos todos los registros de asistencia del empleado
            attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', contract.employee_id.id), 
                ('check_in', '>=', payslip.date_from), ('check_out', '<=', payslip.date_to)])
            
            dias_trabajados = 0
            for attendance_id in attendance_ids:
                # Consultar el día de la semana de la asistencia en base a la hora de entrada
                attendance_day = datetime.weekday(attendance_id.check_in)

                # Consultar el horario de entrada del dia correspondiente a la asistencia
                calendar_day = contract.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == attendance_day)
                
                if not calendar_day:
                    continue
                dias_trabajados += 1
                
            return dias_trabajados + dias_fin_de_semana
        
        else:
            total_days = self.calcular_dias_trabajados_manual(payslip)
            """total_days = (payslip.date_to - payslip.date_from).days + 1
            month_days = monthrange(payslip.date_to.year, payslip.date_to.month)[1]
            
            if total_days > 30:
                total_days = 30
            if total_days == 16:
                total_days = 15
            
            if contract.schedule_pay == 'monthly':
                if payslip.date_from.day > 15 and payslip.date_to.day == 31:
                    total_days = (payslip.date_to - payslip.date_from).days
            elif contract.schedule_pay == 'bi-weekly':
                total_days = 15
            elif contract.schedule_pay == 'weekly':
                total_days = 6
                if contract.pay_dominical:
                    total_days += 1
            elif contract.schedule_pay == 'decenal':
                total_days = 10
            elif contract.schedule_pay == 'catorcenal':
                total_days = 14"""
            
            return total_days
    
    def calcular_cesantia(self, contract):
        return 10
    
    """
        Método para el reporte de la colilla
    """
    def get_all_leaves_report(self, payslip, employee):
        leaves_list = self.get_all_leaves(payslip, employee)
        return leaves_list[0] + leaves_list[1]
    
    def validate_end_of_month(self, payslip):
        year = payslip.date_from.year
        month = payslip.date_from.month
        first_day = datetime.strptime('%s-%s-01' % (year, month), '%Y-%m-%d').date()
        total_days = payslip.date_to - first_day
        if total_days.days >= 27 and payslip.dict.contract_schedule_pay != 'monthly':
            first_payslip_id = self.env['hr.payslip'].search([('date_from', '=', first_day), ('employee_id', '=', payslip.employee_id)], limit=1)
            
            total_devengado = 0
            ibc = 0
            if first_payslip_id:
                total_devengado = first_payslip_id.line_ids.filtered(lambda x: x.code == 'Total_Devengado').amount
                ibc = first_payslip_id.line_ids.filtered(lambda x: x.code == 'IBC').amount
            return (True, total_devengado, ibc)
        else:
            return (False, 0, 0)
    
    def validar_recargo_end_month(self, payslip):
        year = payslip.date_from.year
        month = payslip.date_from.month
        first_day = datetime.strptime('%s-%s-01' % (year, month), '%Y-%m-%d').date()
        total_days = payslip.date_to - first_day
        if total_days.days >= 27 and payslip.dict.contract_schedule_pay != 'monthly':
            first_payslip_id = self.env['hr.payslip'].search([('date_from', '=', first_day), ('employee_id', '=', payslip.employee_id)], limit=1)
            
            recargo = 0
            if first_payslip_id:
                recargo = first_payslip_id.line_ids.filtered(lambda x: x.code == 'Recargo').amount
            return (True, recargo)
        else:
            return (False, 0)

    def calcular_riesgo_profesional(self, employee, IBC, tipo, Novedades, VacacionesAnticipadas, VacacionesDisfrutadas):
        # pagar = self.pagar_ccf_sena_icfb_arl(payslip, employee)
        #if pagar:
        total = 0
        if employee.job_id and employee.job_id.riesgos_profesionales.name == tipo:
            total = (IBC - Novedades - VacacionesAnticipadas - VacacionesDisfrutadas) * employee.job_id.riesgos_profesionales.tarifa
        return round(total, 2)
        #else:
            #return 0
    
    def pagar_ccf_sena_icfb_arl(self, payslip, employee):
        return True
        ausencias = self._get_ausencias(payslip, employee)
        for ausencia in ausencias:
            if ausencia.holiday_status_id.leave_type != 'ausencia_remunerada':
                return False
        return True
    
    def calcular_salario_promedio_vacaciones(self, payslip):
        # TODO: Validar si en calculo por quincen, semana etc, se considera el pago mensual o en base a pago planificado
        date_from_last_year = payslip.date_from - relativedelta(years=1)
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '<', payslip.date_from), 
            ('date_from', '>=', date_from_last_year),
            ('employee_id', '=', payslip.employee_id)
        ])
        
        total_devengado_salarial = 0
        dias_trabajados = 0
        for pay in payslip_ids:
            dias_trabajados += pay.x_dias_trabajados
            for line in pay.line_ids.filtered(lambda x: x.category_id.code == 'DevengadoSalarial' and 
                                              x.code not in ['VacacionesCompensadas', 'VacacionesDisfrutadas', 'VacacionesAnticipadas']):
                total_devengado_salarial += line.amount
        if dias_trabajados <= 0:
            return [0, dias_trabajados]
        return [total_devengado_salarial / dias_trabajados * 30, dias_trabajados]
        
    
    # Vacaciones ANUALES
    def consultar_vacaciones_disfrutadas(self, payslip, employee):
        vacaciones = self._get_ausencias(payslip, employee)
        if vacaciones:
            dias = 0
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type == 'vacaciones_anuales':
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if vac.request_date_from <= payslip.date_from and vac.request_date_to <= payslip.date_to:            
                        dias += (vac.request_date_to - payslip.date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to <= payslip.date_to:
                        dias += (vac.request_date_to - vac.request_date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, vac.request_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - vac.request_date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, vac.request_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.request_date_from <= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, payslip.date_from)
            return (payslip.dict.contract_id.wage / 30) * dias
        else:
            return 0
        
    def consultar_vacaciones_anticipadas(self, payslip, employee):
        value, dias = self.calcular_salario_promedio_vacaciones(payslip)
        vacaciones = self._get_ausencias(payslip, employee)
        if vacaciones:
            dias = 0
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type == 'vacaciones_anticipadas':
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if vac.request_date_from <= payslip.date_from and vac.request_date_to <= payslip.date_to:            
                        dias += (vac.request_date_to - payslip.date_from).days + 1
                        #dias -= self._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to <= payslip.date_to:
                        dias += (vac.request_date_to - vac.request_date_from).days + 1
                        #dias -= self._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, vac.request_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - vac.request_date_from).days + 1
                        #dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, vac.request_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.request_date_from <= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1
                        #dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, payslip.date_from)
            # return (payslip.dict.contract_id.wage / 30) * dias
            return (payslip.dict.contract_id.wage + value) * dias / 30
        else:
            return 0
    
    def consultar_vacaciones_compensadas(self, payslip, employee):
        vacaciones = self._get_ausencias(payslip, employee)
        if vacaciones:
            dias = 0
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type in ['vacaciones_anuales', 'vacaciones_anticipadas'] and vac.compensar_vacaciones:
                    if vac.compensar_date_from <= payslip.date_from and vac.compensar_date_to <= payslip.date_to:            
                        dias += (vac.compensar_date_to - payslip.date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.compensar_date_from >= payslip.date_from and vac.compensar_date_to <= payslip.date_to:
                        dias += (vac.compensar_date_to - vac.compensar_date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, vac.compensar_date_to, vac.compensar_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.compensar_date_from >= payslip.date_from and vac.compensar_date_to >= payslip.date_to:
                        dias += (payslip.date_to - vac.compensar_date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, vac.compensar_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.compensar_date_from <= payslip.date_from and vac.compensar_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1
                        dias -= self._descontar_festivos_y_no_laborales(payslip, payslip.date_to, payslip.date_from)
            return (payslip.dict.contract_id.wage / 30) * dias
        else:
            return 0
    
    def _descontar_festivos_y_no_laborales(self, payslip, date_to, date_from):
        fecha_control = date_from
        festivos_y_no_laborables = 0
        
        for n in range(0, (date_to - date_from).days + 1):
            day_of_week = datetime.weekday(fecha_control)
            calendar_day = payslip.dict.contract_id.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == day_of_week)
                
            if not calendar_day:
                festivos_y_no_laborables += 1
            
            # Consultar si es dia festivo
            dia_festivo = self._cosultar_dia_festivo(payslip, fecha_control)
            
            if dia_festivo:
                festivos_y_no_laborables += 1
            fecha_control += timedelta(days=1)
        return festivos_y_no_laborables
