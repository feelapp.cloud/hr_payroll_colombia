# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'
    
    referencia_colilla = fields.Selection([
        ('0', 'Pago 1ra Quincena'),
        ('1', 'Pago 2da Quincena'),
        ('2', 'Pago 1ra Semana'),
        ('3', 'Pago 2da Semana'),
        ('4', 'Pago 3ra Semana'),
        ('5', 'Pago 4ta Semana'),
        ('6', 'Pago 1er Catorcenal'),
        ('7', 'Pago 2do Catorcenal'),
        ('8', 'Pago Mensual'),
        ('9', 'Pago 1 Decenal'),
        ('10', 'Pago 2 Decenal'),
        ('11', 'Pago 3 Decenal'),
        ('12', 'Liquidación'),
        ('13', 'Días')
    ], string='Referencia Colilla', default='0', required=True)


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'
    
    @api.multi
    def compute_sheet(self):
        print("Hola")
        res = super(HrPayslipEmployees, self).compute_sheet()
        payslip_run_id = self.env['hr.payslip.run'].search([('id', '=', self.env.context.get('active_id'))])
        for item in self.env['hr.payslip'].search([('payslip_run_id', '=', payslip_run_id.id)]):
            item.write({
                'referencia_colilla': payslip_run_id.referencia_colilla
            })
        return res