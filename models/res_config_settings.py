# -*- coding: utf-8 -*-

from re import I
from odoo import api, models, fields


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    pago_cesantias = fields.Date(string='Fecha Pago Cesantia', default='2021-02-05')
    pago_interes_cesantias = fields.Date(string='Fecha Pago I/Cesantias', default='2021-02-05')
    pago_prima1 = fields.Date(string='Fecha Pago Prima 1', default='2021-06-05')
    pago_prima2 = fields.Date(string='Fecha Pago Prima 2', default='2021-12-05')
    # Alertas
    alerta_pago_cesantias = fields.Date(string='Alerta Pago Cesantia')
    alerta_pago_interes_cesantias = fields.Date(string='Alerta Pago I/Cesantias')
    alerta_pago_prima1 = fields.Date(string='Alerta Pago Prima 1')
    alerta_pago_prima2 = fields.Date(string='Alerta Pago Prima 2')
    #auxilio_transporte = fields.Char(string='Auxilio Transporte')
    manual_payroll = fields.Boolean(string='Nómina Manual')
    parametro_ccf = fields.Float(related='company_id.parametro_ccf', string='Parámetro CCF', readonly=False)
    parametro_icbf = fields.Float(related='company_id.parametro_icbf', string='Parámetro ICBF', readonly=False)
    parametro_sena = fields.Float(related='company_id.parametro_sena', string='Parámetro SENA', readonly=False)
    parametro_pension = fields.Selection(related='company_id.parametro_pension', string='Parámetro pensión', defaul='12', readonly=False)
    estructura_liquidacion_id = fields.Many2one(related='company_id.estructura_liquidacion_id', string='Estructura liquidación', readonly=False)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrCPSudo = self.env['ir.config_parameter'].sudo()
        res.update(
            pago_cesantias=IrCPSudo.get_param('hr_payroll_colombia.pago_cesantias'),
            pago_interes_cesantias=IrCPSudo.get_param('hr_payroll_colombia.pago_interes_cesantias'),
            pago_prima1=IrCPSudo.get_param('hr_payroll_colombia.pago_prima1'),
            pago_prima2=IrCPSudo.get_param('hr_payroll_colombia.pago_prima2'),
            alerta_pago_cesantias=IrCPSudo.get_param('hr_payroll_colombia.alerta_pago_cesantias'),
            alerta_pago_interes_cesantias=IrCPSudo.get_param('hr_payroll_colombia.alerta_pago_interes_cesantias'),
            alerta_pago_prima1=IrCPSudo.get_param('hr_payroll_colombia.alerta_pago_prima1'),
            alerta_pago_prima2=IrCPSudo.get_param('hr_payroll_colombia.alerta_pago_prima2'),
            #auxilio_transporte=IrCPSudo.get_param('hr_payroll_colombia.auxilio_transporte'),
            manual_payroll=IrCPSudo.get_param('hr_payroll_colombia.manual_payroll'),
        )
        return res
    
    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrCPSudo = self.env['ir.config_parameter'].sudo()
        IrCPSudo.set_param('hr_payroll_colombia.pago_cesantias', self.pago_cesantias)
        IrCPSudo.set_param('hr_payroll_colombia.pago_interes_cesantias', self.pago_interes_cesantias)
        IrCPSudo.set_param('hr_payroll_colombia.pago_prima1', self.pago_prima1)
        IrCPSudo.set_param('hr_payroll_colombia.pago_prima2', self.pago_prima2)
        IrCPSudo.set_param('hr_payroll_colombia.alerta_pago_cesantias', self.alerta_pago_cesantias)
        IrCPSudo.set_param('hr_payroll_colombia.alerta_pago_interes_cesantias', self.alerta_pago_interes_cesantias)
        IrCPSudo.set_param('hr_payroll_colombia.alerta_pago_prima1', self.alerta_pago_prima1)
        IrCPSudo.set_param('hr_payroll_colombia.alerta_pago_prima2', self.alerta_pago_prima2)
        #IrCPSudo.set_param('hr_payroll_colombia.auxilio_transporte', self.auxilio_transporte)
        IrCPSudo.set_param('hr_payroll_colombia.manual_payroll', self.manual_payroll)
