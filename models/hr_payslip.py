# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import api, models, fields


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'
    _order = 'date_from desc, employee_id asc, number desc'
    
    REFERENCIAS = {
        '0': 'Pago 1ra Quincena',
        '1': 'Pago 2da Quincena',
        '2': 'Pago 1ra Semana',
        '3': 'Pago 2da Semana',
        '4': 'Pago 3ra Semana',
        '5': 'Pago 4ta Semana',
        '6': 'Pago 1er Catorcenal',
        '7': 'Pago 2do Catorcenal',
        '8': 'Pago Mensual',
        '9': 'Pago 1 Decenal',
        '10': 'Pago 2 Decenal',
        '11': 'Pago 3 Decenal',
        '12': 'Liquidación',
        '13': 'Días'
    }
    
    contract_schedule_pay = fields.Selection(related='contract_id.contract_schedule_pay', string='Pago Por Contrato')
    febrary_month = fields.Boolean(string='Mes Febrero', compute='_validate_febrary_month', default=False)
    pay_days = fields.Boolean(string='Pago por Días', default=False)
    referencia_colilla = fields.Selection([
        ('0', 'Pago 1ra Quincena'),
        ('1', 'Pago 2da Quincena'),
        ('2', 'Pago 1ra Semana'),
        ('3', 'Pago 2da Semana'),
        ('4', 'Pago 3ra Semana'),
        ('5', 'Pago 4ta Semana'),
        ('6', 'Pago 1er Catorcenal'),
        ('7', 'Pago 2do Catorcenal'),
        ('8', 'Pago Mensual'),
        ('9', 'Pago 1 Decenal'),
        ('10', 'Pago 2 Decenal'),
        ('11', 'Pago 3 Decenal'),
        ('12', 'Liquidación'),
        ('13', 'Días')
    ], string='Referencia Colilla', default='0', required=True)
    x_dias_trabajados = fields.Integer(string='Días trabajados', readonly=True, default=0)
    
    def consultar_smlv(self):
        return self.env.user.company_id.smmlv_value
    
    def consultar_porcentaje_icbf(self):
        return self.env.user.company_id.parametro_icbf / 100
    
    def consultar_porcentaje_ccf(self):
        return self.env.user.company_id.parametro_ccf / 100
    
    def consultar_porcentaje_sena(self):
        return self.env.user.company_id.parametro_sena / 100
    
    def consultar_auxilio_transporte_parametro(self):
        return self.env.user.company_id.stm_value
    
    @api.depends('date_from')
    def _validate_febrary_month(self):
        for item in self:
            if item.date_from.month == 2:
                item.febrary_month = True

    @api.model
    def get_worked_day_lines(self, contracts, date_from, date_to):
        res = super(HrPayslip, self).get_worked_day_lines(contracts, date_from, date_to)
        for item in res:
            if 'WORK100' in item['code']:
                item['code'] = 'DiasTrabajados'
        return res
    
    @api.onchange('employee_id')
    def _get_bono_rodamiento(self):
        if self.employee_id:
            self.input_line_ids = [(5, 0, {})]
            adicionales = []

            # Bono
            adicionales = [(0, 0, {
                'name': 'Bono',
                'code': 'Bono',
                'amount': 0,
                #'contract_id': self.employee_id.contract_id.id
            })]

            # Rodamiento
            adicionales += [(0, 0, {
                'name': 'Rodamiento',
                'code': 'Rodamiento',
                'amount': 0,
                #'contract_id': self.employee_id.contract_id.id
            })]

            # Prestamo
            adicionales += [(0, 0, {
                'name': 'Prestamo',
                'code': 'DEUDA',
                'amount': 0,
                #'contract_id': self.employee_id.contract_id.id
            })]

            self.input_line_ids = adicionales
    
    def action_payslip_done(self):
        res = super(HrPayslip, self).action_payslip_done()
        cesantias_mes = self.line_ids.filtered(lambda x: x.code == 'Cesantias_mensual')
        if cesantias_mes:
            self.employee_id.write({
                'total_cesantias': cesantias_mes.total
            })
        return res
    
    def referencia_colilla_report(self, payslip):
        meses = {
            '01': 'Enero',
            '02': 'Febrero',
            '03': 'Marzo',
            '04': 'Abril',
            '05': 'Mayo',
            '06': 'Junio',
            '07': 'Julio',
            '08': 'Agosto',
            '09': 'Septiembre',
            '10': 'Octubre',
            '11': 'Noviembre',
            '12': 'Diciembre',
        }
        return self.REFERENCIAS[payslip.referencia_colilla] + ' ' + meses['%s' % str(payslip.date_from.strftime('%m'))] + ' ' + payslip.date_from.strftime('%Y')
    
    def dias_ausencia_por_luto(self, rule):
        return int(rule.total / (self.contract_id.wage / 30))
    
    def calcular_vacaciones_compensadas_dias(self, rule):
        return int(rule.total / (self.contract_id.wage / 30))
    
    def calcular_vacaciones_disfrutadas_dias(self, rule):
        return int(rule.total / (self.contract_id.wage / 30))
    
    def _descontar_festivos_y_no_laborales(self, payslip, date_to, date_from):
        fecha_control = date_from
        festivos_y_no_laborables = 0
        
        for n in range(0, (date_to - date_from).days + 1):
            day_of_week = datetime.weekday(fecha_control)
            calendar_day = payslip.contract_id.resource_calendar_id.attendance_ids.filtered(lambda x: int(x.dayofweek) == day_of_week)
                
            if not calendar_day:
                festivos_y_no_laborables += 1
            
            # Consultar si es dia festivo
            dia_festivo = payslip.contract_id._cosultar_dia_festivo(payslip, fecha_control)
            
            if dia_festivo:
                festivos_y_no_laborables += 1
            fecha_control += timedelta(days=1)
        return festivos_y_no_laborables
    
    def calcular_vacaciones_anticipadas_dias(self, payslip, type):
        #return int(rule.total / (self.contract_id.wage / 30))
        contract = payslip.contract_id
        vacaciones = contract._get_ausencias(payslip, payslip.employee_id)
        if vacaciones:
            dias = 0
            no_laborales = 0
            compensadas = {
                'total': 0, 
                'no_habiles': 0
            }
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type == type:
                    # Si la ausencia inicia antess del inicio de nomina y termina antes de de la fecha final
                    if vac.request_date_from <= payslip.date_from and vac.request_date_to <= payslip.date_to:            
                        dias += (vac.request_date_to - payslip.date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to <= payslip.date_to:
                        dias += (vac.request_date_to - vac.request_date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, vac.request_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.request_date_from >= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - vac.request_date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, payslip.date_to, vac.request_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.request_date_from <= payslip.date_from and vac.request_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, payslip.date_to, payslip.date_from)

                    # Si hay dias compensados de vacaciones descontar
                    if vac.holiday_status_id.leave_type == type and vac.compensar_vacaciones:
                        compensadas = payslip.consultar_vacaciones_compensadas(payslip)
            # return (payslip.dict.contract_id.wage / 30) * dias
            return {
                'total': dias - compensadas['total'], 
                'no_habiles': no_laborales - compensadas['no_habiles'],
            }
        else:
            return {
                'total': 0, 
                'no_habiles': 0,
            }
    
    def consultar_vacaciones_compensadas(self, payslip):
        contract = payslip.contract_id
        vacaciones = contract._get_ausencias(payslip, payslip.employee_id)
        if vacaciones:
            dias = 0
            no_laborales = 0
            for vac in vacaciones:
                if vac.holiday_status_id.leave_type in ['vacaciones_anuales', 'vacaciones_anticipadas'] and vac.compensar_vacaciones:
                    if vac.compensar_date_from <= payslip.date_from and vac.compensar_date_to <= payslip.date_to:            
                        dias += (vac.compensar_date_to - payslip.date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, vac.request_date_to, payslip.date_from)
                    # Si la ausencia esta dentro del periodo de la nomina
                    elif vac.compensar_date_from >= payslip.date_from and vac.compensar_date_to <= payslip.date_to:
                        dias += (vac.compensar_date_to - vac.compensar_date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, vac.compensar_date_to, vac.compensar_date_from)
                    # Si inicio de la ausencia esta dentro del periodo de nomina y el final despues
                    elif vac.compensar_date_from >= payslip.date_from and vac.compensar_date_to >= payslip.date_to:
                        dias += (payslip.date_to - vac.compensar_date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, payslip.date_to, vac.compensar_date_from)
                    # Si la ausencia inicia antes o termina despues del periodo de la nomina
                    elif vac.compensar_date_from <= payslip.date_from and vac.compensar_date_to >= payslip.date_to:
                        dias += (payslip.date_to - payslip.date_from).days + 1
                        no_laborales += payslip._descontar_festivos_y_no_laborales(payslip, payslip.date_to, payslip.date_from)
            return {
                'total': dias, 
                'no_habiles': no_laborales
            }
        else:
            return {
                'total': 0, 
                'no_habiles': 0
            }
    
    def calcular_base_salario(self, contract, Bonificacion):
        total_adicionales = 0
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', contract.date_start),
            ('date_from', '<=', contract.date_end),
            ('employee_id', '=', contract.employee_id.id)
        ])
        #payslip_ids = self.env['hr.payslip.line'].search([
        #    ('slip_id.date_from', '>=', contract.date_start),
        #    ('slip_id.date_from', '<=', contract.date_end),
        #    ('salary_rule_id.code', '=', 'BonificacionPrestacional'),
        #    ('slip_id.employee_id', '=', contract.employee_id.id)
        #])
        
        for pay in payslip_ids:
            total_adicionales += pay.line_ids.filtered(lambda x: x.code == 'Pago_constitutivo').amount - pay.line_ids.filtered(lambda x: x.code == 'Sueldo').amount
            #total_bonificaciones += pay.filtered(lambda x: x.code == 'BonificacionPrestacional').amount
        
        total_dias = self.calcular_total_dias_trabajados(contract)
        return contract.wage + ((total_adicionales + Bonificacion) / total_dias * 30)
    
    def sumar_ingresos_salariales(self, contract, DevengadoSalarial):
        total = 0
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', contract.date_start),
            ('date_from', '<=', contract.date_end),
            ('employee_id', '=', contract.employee_id.id)
        ])
        #payslip_ids = self.env['hr.payslip.line'].search([
        #    ('slip_id.date_from', '>=', contract.date_start),
        #    ('slip_id.date_from', '<=', contract.date_end),
        #    ('salary_rule_id.code', '=', 'BonificacionPrestacional'),
        #    ('slip_id.employee_id', '=', contract.employee_id.id)
        #])
        
        for pay in payslip_ids:
            for sal in pay.line_ids.filtered(lambda x: x.category_id.code == 'DevengadoSalarial'):
                total += sal.amount
            #total_bonificaciones += pay.filtered(lambda x: x.code == 'BonificacionPrestacional').amount
        
        return total + DevengadoSalarial
    
    def calcular_base_liquidacion(self, contract, DevengadoSalarial):
        ingresos_salariales = self.sumar_ingresos_salariales(contract, DevengadoSalarial)
        salario_promedio = ingresos_salariales / contract.dias_trabajados * 30
        
        if (contract.wage + salario_promedio) <= self.consultar_smlv() * 2:
            return (contract.wage + salario_promedio) + self.consultar_auxilio_transporte_parametro()    
        return contract.wage + salario_promedio
    
    # Base Salarial
    def calcular_base_liquidacion_vacaciones(self, contract, DevengadoSalarial):
        ingresos_salariales = self.sumar_ingresos_salariales(contract, DevengadoSalarial)
        salario_promedio = ingresos_salariales / contract.dias_trabajados * 30
   
        return contract.wage + salario_promedio

    """def consultar_sueldo_vacaciones_provisionadas(self, payslip, PagoConstitutivo):
        if payslip.date_from == payslip.dict.contract_id.date_start:
            return payslip.dict.contract_id.wage
        else:
            return PagoConstitutivo"""
    
    def calcular_vacaciones_pendientes(self, contract, Bonificacion):
        # Días trabajados durante el contrato
        total_dias = self.calcular_total_dias_trabajados(contract)
        
        # Dias proporcionales de vacaciones
        vacaciones = total_dias * 15 / 360
        
        total_base_salario = self.calcular_base_salario(contract, Bonificacion)
        
        return total_base_salario / 30 * vacaciones
    
    def calcular_total_dias_trabajados(self, contract):
        total_dias = 0
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', contract.date_start),
            ('date_from', '<=', contract.date_end),
            ('employee_id', '=', contract.employee_id.id)
        ])
        for pay in payslip_ids:
            total_dias += pay.contract_id.get_worked_days(pay, pay.contract_id) - pay.contract_id.get_all_leaves_report(pay, pay.employee_id.id)
        return total_dias

    def calcular_cesantias_liquidacion(self, contract, BaseSalarial):
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', contract.date_start),
            ('date_from', '<=', contract.date_end),
            ('employee_id', '=', contract.employee_id.id)
        ])
        amount_no_justificada = 0
        for pay in payslip_ids:
            amount_no_justificada += pay.contract_id.get_all_leaves(pay, pay.employee_id.id)[1]
        return ((BaseSalarial) * (contract.dias_trabajados - amount_no_justificada)) / 360
    
    def consultar_cesantias_liquidacion_base(self):
        BaseSalarial = self.line_ids.filtered(lambda x: x.code == 'BaseSalarial').amount
        # AuxilioTransporte = self.line_ids.filtered(lambda x: x.code == 'Aux_Transporte').amount
        return (BaseSalarial) * self.contract_id.dias_trabajados / 360
        # return (BaseSalarial + AuxilioTransporte) * self.contract_id.dias_trabajados / 360
    
    def calcular_prima_liquidacion(self, contract, BaseSalarial):
        return ((BaseSalarial) * (contract.dias_trabajados)) / 360

    def calcular_cesantias_interes_liquidacion(self, contract, Cesantias):
        total_dias = self.calcular_total_dias_trabajados(contract)
        return (Cesantias * total_dias * 0.12) / 360
    
    def calcular_pension_por_ausencia(self, payslip, employee, porcentaje):
        leaves = payslip.contract_id.get_all_leaves(payslip, employee)
        # return (leaves[0] + leaves[1]) * ((payslip.contract_id.wage / 30) * porcentaje)
        return (leaves[1]) * ((payslip.contract_id.wage / 30) * porcentaje)
        
        
        # fecha_control = contract.date_from
        """for i in range(1, int(contract.date_end - contract.date_start).days / 30) + 1:
            fecha_fin_mes = datetime.strptime('%s-%s-30' % (fecha_control.year, fecha_control.month), '%Y-%m-%d')
            if fecha_control.month != 2:
                # Si la fecha del contrato es menor a la fecha final del mes, usar fecha del contrato como base
                if fecha_fin_mes >= contract.date_end:
                    fecha_fin_mes = contract.date_end
                
            return 10"""
        """payslip_ids = self.env['hr.payslip.line'].search([
            ('slip_id.date_from', '>=', payslip.contract_id.date_start),
            ('slip_id.date_from', '<=', payslip.contract_id.date_end),
            ('salary_rule_id.code', '=', 'BonificacionPrestacional'),
            ('employee_id', '=', payslip.employee_id)
        ])
        return 10"""
    
    def compute_sheet(self):
        res = super(HrPayslip, self).compute_sheet()
        
        for payslip in self:
            payslip.write({
                'x_dias_trabajados': payslip.contract_id.get_dias_trabajados_con_vacaciones(payslip)
            })
            
            # Actualizar valores en contrato
            payslip.contract_id.write({
                'salario_promedio': payslip.line_ids.filtered(lambda x: x.code == 'BaseSalarial').amount,
                'base_vacaciones': payslip.consultar_cesantias_liquidacion_base(),
                'base_cesantias': payslip.line_ids.filtered(lambda x: x.code == 'CesantiasLiquidacion').amount,
                'base_prima': payslip.line_ids.filtered(lambda x: x.code == 'PrimaLiquidacion').amount,
                'liq_vacaciones': payslip.line_ids.filtered(lambda x: x.code == 'VacacionesLiquidacion').amount,
                'liq_cesantias': payslip.line_ids.filtered(lambda x: x.code == 'CesantiasLiquidacion').amount,
                'liq_intereses_cesantias': payslip.line_ids.filtered(lambda x: x.code == 'CesantiasInteresLiquidacion').amount,
                'liq_prima': payslip.line_ids.filtered(lambda x: x.code == 'PrimaLiquidacion').amount,
                'liq_otros': payslip.sumar_ingresos_salariales(payslip.contract_id, payslip.line_ids.filtered(lambda x: x.code == 'DevengadoSalarial').amount),
                'liq_total': payslip.line_ids.filtered(lambda x: x.code == 'TotalLiquidacion').amount,
            })
        return res

    # CALCULO DE REGLAS
    
    def calcular_auxilio_transporte(self, payslip, contract, Sueldo, Pago_constitutivo, Pago_no_constitutivo, SMLV_Parametro, Aux_Transporte_Parametro):
        # El Total Devengado No puede exceder de 2 salarios minimos
        segunda, recargo = self.env['hr.contract'].validar_recargo_end_month(payslip)
        
        # TODO: Buscar otros ingresos del mes para sumarlos al actual, y validar si pagar o no el Auxilio de Transporte
        past_payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '<=', payslip.date_from),
            ('date_from', '>=', payslip.date_from.replace(day=1)),
            ('id', 'not in', [payslip.id]),
            ('employee_id', '=', payslip.employee_id)
        ])
        ingresos_anteriores = 0
        for pay in past_payslip_ids:
            ingresos_anteriores += pay.line_ids.filtered(lambda x: x.code == 'Pago_no_constitutivo').amount or 0
            ingresos_anteriores += (pay.line_ids.filtered(lambda x: x.code == 'Pago_constitutivo').amount or 0) - pay.line_ids.filtered(lambda x: x.code == 'Sueldo').amount

        # control = True if (Pago_constitutivo + Pago_no_constitutivo + ingresos_anteriores) <= (2 * SMLV_Parametro) else False
        control = True if (contract.wage + Pago_constitutivo - Sueldo + Pago_no_constitutivo + ingresos_anteriores) <= (2 * SMLV_Parametro) else False
        
        if control and contract.pagar_auxilio_transporte:
            if self.env['ir.config_parameter'].get_param('hr_payroll_colombia.manual_payroll'):
                dias_ausencia = self.env['hr.contract'].get_all_leaves(payslip, payslip.employee_id)
                total_ausencias = dias_ausencia[0] + dias_ausencia[1]
                auxilio_por_dia = Aux_Transporte_Parametro / 30
                return round((auxilio_por_dia * self.env['hr.contract'].calcular_dias_trabajados_manual(payslip)) - (auxilio_por_dia * total_ausencias), 0)
                # return round((auxilio_por_dia * self.env['hr.contract'].total_period_days(payslip)) - (auxilio_por_dia * total_ausencias), 0)
            else:
                return round(((Aux_Transporte_Parametro / 30) * self.env['hr.contract'].get_worked_days(payslip, contract)), 0)
        else:
            return 0
    
    def sumar_ibc_contrato(self, payslip):
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', payslip.contract_id.date_start),
            ('date_from', '<=', payslip.contract_id.date_end),
            ('employee_id', '=', payslip.employee_id),
            ('state', '!=', 'cancel')
        ])
        
        ibc = 0
        for pay in payslip_ids:
            ibc += pay.line_ids.filtered(lambda x: x.code == 'IBC').total
        return ibc
    
    def sumar_ausencias_no_remunerada(self, payslip):
        payslip_ids = self.env['hr.payslip'].search([
            ('date_from', '>=', payslip.contract_id.date_start),
            ('date_from', '<=', payslip.contract_id.date_end),
            ('employee_id', '=', payslip.employee_id),
            ('state', '!=', 'cancel')
        ])
        
        ausencia = 0
        for pay in payslip_ids:
            ausencia += pay.line_ids.filtered(lambda x: x.code == 'Ausencia_No_Remunerada').total
        return ausencia
    
    def calcular_devengado_liquidacion(self, payslip, ausencia_amount):
        total_ausencias = self.sumar_ausencias_no_remunerada(payslip) + ausencia_amount
        return self.sumar_ibc_contrato(payslip) - total_ausencias
        
    def calcular_base_salarial(self, payslip, ausencia_amount):
        #  total_ausencias = self.sumar_ausencias_no_remunerada(payslip) + ausencia_amount
        ibc = self.sumar_ibc_contrato(payslip)
        return (ibc * 30) / payslip.contract_id.dias_trabajados
    
    def calcular_vacaciones_liquidacion(self, payslip, BaseVacaciones):
        return (BaseVacaciones * payslip.contract_id.dias_vacaciones) / 30        
        
        """# Buscar registros de asistencia
        attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', self.contract_id.employee_id.id), 
            ('check_in', '>=', self.date_from), ('check_out', '<=', self.date_to)])
        
        calendar_id = self.contract_id.resource_calendar_id.attendance_ids
        total_days = (self.date_to - self.date_from).days + 1
        
        non_attendance_total = 0
        non_attendance_per_week = 0
        non_attendance_days = []
        
        for i in range(0, total_days):
            current_day = self.date_from + timedelta(days=i)
            week_day = datetime.weekday(current_day)
            
            # Validar si es día laboral
            if calendar_id.filtered(lambda x: x.dayofweek == str(week_day)):
                # Validar si asistio
                if not attendance_ids.filtered(lambda x: x.check_in.date() == current_day):
                    non_attendance_per_week += 1
                    non_attendance_days.append(current_day)
            
            if week_day == 6:
                if non_attendance_per_week:
                    non_attendance_total = non_attendance_per_week + 1
                    non_attendance_per_week = 0
                    non_attendance_days.append(current_day)
        
        # Crear asusencias
        if non_attendance_days:
            HrLeave = self.env['hr.leave']
            for item in non_attendance_days:
                if not HrLeave.search([('non_attendance_leave', '=', True), ('non_attendance_date', '=', item)]):
                    HrLeave.create({
                        'holiday_status_id': self.env.ref('hr_payroll_colombia.hr_leave_type_no_paid').id,
                        'request_date_from': item,
                        'request_date_to': item,
                        'date_from': item + timedelta(days=1),
                        'date_to': item + timedelta(days=1),
                        'name': 'Ausencia No Pagada',
                        'employee_id': self.employee_id.id,
                        'non_attendance_leave': True,
                        'number_of_days': 1,
                        'non_attendance_date': item
                    })
        return res"""
        
    # def calcular_cesantia(self, contract):
        # fecha_inicial = datetime.strptime('01/01/%s' % str(contract.fecha_pago_cesantias.year - 1), '%m/%d/%Y').date()
        # fecha_final = datetime.strptime('12/31/%s' % str(contract.fecha_pago_cesantias.year - 1), '%m/%d/%Y').date()

        
            # return 10


class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'
    
    # Modificar el metodo original para no considerar cantidades ni porcentaje
    @api.depends('quantity', 'amount', 'rate')
    def _compute_total(self):
        for line in self:
            line.total = line.amount
            # line.write({'quantity': 10})
            # line.total = float(line.quantity) * line.amount * line.rate / 100
