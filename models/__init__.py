# -*- coding: utf-8 -*-

from . import hr_contract
from . import hr_employee
from . import hr_holidays_calendar
from . import hr_job
from . import hr_leave
from . import hr_leave_type
from . import hr_payroll_account
from . import hr_payroll_structure
from . import hr_payslip
from . import hr_salary_rule
from . import hr_payslip_run
from . import res_company
from . import res_config_settings
