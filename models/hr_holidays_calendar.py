# -*- coding: utf-8 -*-

from odoo import models, fields


class HrHolidaysCalendar(models.Model):
    _name = 'hr.holidays.calendar'
    _description = 'Calendario de dias festivos'
    
    name = fields.Char(string='Nombre', required=True)
    day = fields.Selection([
        ('1', '01'),
        ('2', '02'),
        ('3', '03'),
        ('4', '04'),
        ('5', '05'),
        ('6', '06'),
        ('7', '07'),
        ('8', '08'),
        ('9', '09'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
        ('21', '21'),
        ('22', '22'),
        ('23', '23'),
        ('24', '24'),
        ('25', '25'),
        ('26', '26'),
        ('27', '27'),
        ('28', '28'),
        ('29', '29'),
        ('30', '30'),
        ('31', '31')
    ], string='Día', required=True)
    month = fields.Selection([
        ('1', 'Enero'),
        ('2', 'Febrero'),
        ('3', 'Marzo'),
        ('4', 'Abril'),
        ('5', 'Mayo'),
        ('6', 'Junio'),
        ('7', 'Julio'),
        ('8', 'Agosto'),
        ('9', 'Septiembre'),
        ('10', 'Octubre'),
        ('11', 'Novimienbre'),
        ('12', 'Diciembre')
    ], string='Mes', required=True)
    # date = fields.Date(string='Fecha', required=True)
    # holiday_calendar_line_ids = fields.One2many('hr.holidays.calendar.line', 'holiday_calendar_id', string='Días festivos')
    active = fields.Boolean(string='Activo', default=True)


#class HrHolidaysCalendarLine(models.Model):
#    _name = 'hr.holidays.calendar.line'
#    _description = 'Dias festivos'
    
#    name = fields.Char(string='Nombre', required=True)
#    holiday_calendar_id = fields.Many2one('hr.holidays.calendar', string='Calendario días festivos', ondelete='cascade')
#    date = fields.Date(string='Fecha', required=True)
#    active = fields.Boolean(string='Activo', default=True)
