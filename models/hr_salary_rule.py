# -*- coding: utf-8 -*-

from odoo import models, fields


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    report_category = fields.Selection([
        ('0', 'Horas extras, ordinarias y recargos'),
        ('1', 'Vacaciones, incapacidades y licencias'),
        ('2', 'Ingresos adicionales'),
        ('3', 'Retención y deducciones')
    ], string='Categoría en colilla')
    x_rule_type = fields.Selection([
        ('hr_salary_rule_smlv', 'Salario Minimo Legal Vigente'),
        ('hr_salary_rule_aux', 'Parámetro Auxilio de Transporte'),
        ('hr_salary_rule_salario_integral', 'Salario Integral'),
        ('hr_salary_rule_uvt', 'UVT'),
        ('hr_salary_rule_recargo', 'Recargo'),
        ('hr_salary_rule_salario_basico', 'Salario Básico'),
        ('hr_salary_rule_sueldo', 'Sueldo'),
        ('hr_salary_rule_hora_extra_diurna', 'Hora Extra Diurnas'),
        ('hr_salary_rule_hora_extra_nocturna', 'Hora Extra Nocturna'),
        ('hr_salary_rule_hora_recargo_nocturno', 'Hora Recargo Nocturno'),
        ('hr_salary_rule_hora_extra_diurna_dominical_festivo', 'Hora Extra Diurna Dominical y Festivos'),
        ('hr_salary_rule_hora_recargo_diurno_dominical_festivo', 'Hora Recargo Diurno Dominical y Festivos'),
        ('hr_salary_rule_hora_extra_nocturno_dominical_festivo', 'Hora Extra Nocturno Dominical y Festivos'),
        ('hr_salary_rule_hora_recargo_nocturno_dominical_festivo', 'Hora Recargo Nocturno Dominical y Festivos'),
        ('hr_salary_rule_total_horas_extras', 'Total Horas Extras'),
        ('hr_salary_rule_comision', 'Comisión'),
        ('hr_salary_rule_retencion_fuente', 'Retención en la Fuente'),
        ('hr_salary_rule_bonificacion', 'Bonificación'),
        ('hr_salary_rule_rodamiento', 'Rodamiento'),
        ('hr_salary_rule_pago_no_contitutivo', 'Pago NO Constitutivo'),
        ('hr_salary_rule_pago_contitutivo', 'Pago Constitutivo'),
        ('hr_salary_rule_incapacidad_comun', 'Incapacidad Común'),
        ('hr_salary_rule_ausencia_por_luto', 'Ausencia Por Luto'),
        ('hr_salary_rule_incapacidad_laboral', 'Incapacidad Laboral'),
        ('hr_salary_rule_ausencia_remunerada', 'Ausencia Remunerada'),
        ('hr_salary_rule_total_novedades', 'Total Novedades'),
        ('hr_salary_rule_vacaciones', 'Vacaciones'),
        ('hr_salary_rule_ibc', 'IBC'),
        ('hr_salary_rule_auxilio_transporte', 'Auxilio de Transporte'),
        ('hr_salary_rule_total_devengado', 'Total Devengado'),
        ('hr_salary_rule_pension_empleado', 'Pensión - Empleado'),
        ('hr_salary_rule_salud_empleado', 'Salud - Empleado'),
        ('hr_salary_rule_ausencia_no_remunerada', 'Ausencias No Remunerada'),
        ('hr_salary_rule_prestamo', 'Prestamo'),
        ('hr_salary_rule_fondo_solidaridad_pensional', 'Fondo de Solidaridad Pensional'),
        ('hr_salary_rule_fondo_subsistencia_pensional', 'Fondo de Subsistema Pensional'),
        ('hr_salary_rule_total_deducciones', 'Total Deducciones'),
        ('hr_salary_rule_pension_empleador', 'Pensión - Empleador'),
        ('hr_salary_rule_pension_alto_riesgo_empleador', 'Pensión Alto Riesgo- Empleador'),
        ('hr_salary_rule_salud_empleador', 'Salud - Empleador'),
        ('hr_salary_rule_arl', 'Aporte Riesgo Profesional'),
        ('hr_salary_rule_total_seguridad_social', 'Total Seguridad Social Empresa'),
        ('hr_salary_rule_caja_compensacion_familiar', 'Caja de Compensación Familiar'),
        ('hr_salary_rule_icbf', 'Instituto Colombiano de Bienestar Familiar  ICBF'),
        ('hr_salary_rule_sena', 'Servicio Nacional de Aprendizaje SENA'),
        ('hr_salary_rule_aportaciones_parafiscales', 'Total Parafiscales'),
        ('hr_salary_rule_cesantias_mensual', 'Cesantias mensual'),
        ('hr_salary_rule_prima_servicio', 'Prima Servicio'),
        ('hr_salary_rule_int_cesantias', 'Int / Cesantias'),
        ('hr_salary_rule_total_prestaciones_sociales', 'Total Prestaciones Sociales'),
        ('hr_salary_rule_total_pagar', 'Total a Pagar'),
        ('hr_salary_rule_por_minuto', 'Trabajado por minuto'),
        ('hr_salary_total_costo_empresa', 'Total Costo Empresa'),
        ('hr_salary_rule_total_liquidacion', 'Total Liquidación')
    ], string='Tipo de Regla')
